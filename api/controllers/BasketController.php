<?php

namespace api\controllers;

use common\models\Products;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;
use yii\helpers\Json;

class BasketController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        $products   = [];
        $array      = $_GET['array'];
        $details    = Products::findAll(['id' => $array]);

        $total              = 0;
        $total_count        = 0;

        foreach ($details as $detail) {
            $products[]     = [
                'id'            => $detail->id,
                'name'          => $detail->name,
                'code'          => $detail->code,
                'price'         => $detail->price,
                'description'   => $detail->description,
                'star'          => $detail->star,
                'weight'        => $detail->weight,
                'image'         => $detail->image
                    ? \Yii::$app->request->hostInfo . $detail->getImage()
                    : \Yii::$app->request->hostInfo . '/backend/web/no-image.png',
                'quantity'      => 1,
            ];

            $total              += $detail->price;
            $total_count++;
        }

        return [
            'total_price'   => $total,
            'total_count'   => $total_count,
            'products'      => array_values($products),
        ];
    }

    public function actionResult()
    {
        $array  = $_GET['result'];

        $prices = [];
        foreach ($array as $value){
            $json       = Json::decode($value, true);
            $product    = Products::findOne(['id' => $json['product_id']]);
            $prices[]   = (int) $product->price * (int) $json['count'];
        }

        return array_sum($prices);
    }
}
