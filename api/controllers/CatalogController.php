<?php

namespace api\controllers;

use common\models\Catalog;
use common\models\Products;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class CatalogController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return Products::getProducts();
    }

    public function actionView($id)
    {
        $model  = Catalog::findOne(['id' => $id]);
        $products   = Catalog::get_tree($model->id);

        return Products::getProducts($products);
    }

    public function actionCategory($id)
    {
        return Catalog::getChildCategory($id);
    }

}
