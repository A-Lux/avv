<?php

namespace api\controllers;

use common\models\Cities;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class CityController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return Cities::getAll();
    }

    public function actionView($id)
    {
        return Cities::getOne($id);
    }

}
