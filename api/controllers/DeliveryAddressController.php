<?php

namespace api\controllers;

use common\models\DeliveryAddress;
use common\models\Sales;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class DeliveryAddressController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return DeliveryAddress::getAll();
    }

    public function actionView($id)
    {
        return DeliveryAddress::getOne($id);
    }

}
