<?php

namespace api\controllers;

use api\models\FavoritesForm;
use api\models\ProfileForm;
use common\models\UserFavorites;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

class FavoritesController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['contentNegotiator']      = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return UserFavorites::data();
    }

    public function actionCreate()
    {
        $model = new FavoritesForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $favorite = $model->create()) {
                return [
                    'message'   => $favorite,
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    public function actionDelete()
    {
        $model = new FavoritesForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $favorite = $model->delete()) {
                return [
                    'message'   => $favorite,
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

}