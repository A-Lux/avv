<?php

namespace api\controllers;

use common\models\Catalog;
use common\models\FilterAttribute;
use common\models\FilterValue;
use common\models\Products;
use yii\data\Pagination;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class FilterController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex($category_id)
    {
        return  $this->getFilterAttribute($category_id);
    }

    public function actionProducts()
    {
        $filterValues   = $_GET['value'];
        $category_id    = $_GET['category_id'];

        return $this->getProductsFilter($filterValues, $category_id);
    }

    private function getProductsFilter($filterValues, $category_id)
    {
        $tree   = Catalog::get_tree($category_id);
        $values = FilterValue::find()
            ->where(['in', 'attribute_id', $filterValues])
            ->select('product_id')
            ->distinct('product_id')
            ->all();

        $product_id  = [];
        foreach ($values as $value){
            $product_id[]    = $value->product_id;
        }

        $products   = $this->getPaginationFilter($product_id, $tree);
        $array      = [];

        foreach ($products['data'] as $product){
            $array['data'][]    = [
                'id'        => $product->id,
                'name'      => $product->name,
                'code'      => $product->code,
                'price'     => $product->price,
                'description'   => $product->description,
                'star'          => $product->star,
                'image'         => $product->image
                    ? \Yii::$app->request->hostInfo . $product->getImage()
                    : \Yii::$app->request->hostInfo . '/backend/web/no-image.png',
            ];
        }

        $array['pagination']    = [$products['pagination']];

        return $array;
    }

    private function getFilterValue($id)
    {
        $tree   = Catalog::get_tree($id);
        if (null == $tree) {
            return $tree;
        }

        $query      = Products::find()
            ->where(['in', 'category_id', $tree])
            ->andWhere(['status' => Products::STATUS_ACTIVE])
            ->orderBy(['sort' => SORT_ASC])
            ->all();

        $array  = [];
        foreach ($query as $item){
            $array[] = $item->id;
        }


        return FilterValue::find()
            ->where(['in', 'product_id', $array])
            ->all();
    }

    private function getFilterAttribute($id)
    {
        $attributes     = $this->getFilterValue($id);
        $array          = [];
        $filter         = [];
        foreach ($attributes as $attribute){
            if(!in_array($attribute->attribute_id, $array)) {
                $array[] = $attribute->attribute_id;
            }
        }

        $attributes     = FilterAttribute::find()
            ->where(['in', 'id', $array])
            ->all();
        $countProduct   = 1;

        foreach ($attributes as $attribute){
            $countProduct   = FilterValue::findAll(['id' => $attribute->id]);
            if(!array_key_exists($attribute->entity_id, $filter)) {
                $filter[$attribute->entity_id] = [
                    'id' => $attribute->entity->id,
                    'name' => $attribute->entity->name,
                    'attributes' => [],
                ];

                $filter[$attribute->entity_id]['attributes'][]  = [
                    'id'        => $attribute->id,
                    'name'      => $attribute->name,
                    'count'     => count($countProduct),
                ];
            }else{
                $filter[$attribute->entity_id]['attributes'][]  = [
                    'id'        => $attribute->id,
                    'name'      => $attribute->name,
                    'count'     => count($countProduct), 
                ];
            }
        }

        return array_values($filter);
    }

    private function getPaginationFilter($products_id, $category_id, $pageSize = 12)
    {
        $query      = Products::find()
            ->where(['in', 'id', $products_id])
            ->andWhere(['in', 'category_id', $category_id])
            ->andWhere(['status' => Products::STATUS_ACTIVE])
            ->orderBy(['sort' => SORT_ASC]);

        $count      = $query->count();

        $pagination = new Pagination(['totalCount' => $count, 'pageSize'=>$pageSize]);
        $products   = $query->offset($pagination->offset)
            ->limit($pagination->limit)
            ->all();

        $data['data']       = $products;
        $data['pagination'] = $pagination;

        return $data;
    }

}
