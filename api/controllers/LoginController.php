<?php

namespace api\controllers;

use api\models\LoginForm;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\filters\Cors;

class LoginController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => Cors::className(),
        ];

        $behaviors['contentNegotiator']      = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $model = new LoginForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $customer = $model->auth()) {
                return [
                    'token'     => $customer->token,
                ];
            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        } catch (\Exception $e) {
            throw new HttpException(403, $e->getMessage());
        }
    }
}