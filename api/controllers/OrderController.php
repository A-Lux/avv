<?php

namespace api\controllers;

use api\models\OrderForm;
use common\models\Orders;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class OrderController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'rules' => [
                [
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];

        $behaviors['contentNegotiator'] = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];


        return $behaviors;
    }

    /**
     * view all orders.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionIndex()
    {
        return Orders::getUserItems();
    }

    /**
     * view one order.
     * @param  int $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return Orders::getUserItemId($id);
    }

    public function actionCreate()
    {
        $model  = new OrderForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $order = $model->create()) {
                return [
                    'message'   => 'Благодарим Вас за покупку!',
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}