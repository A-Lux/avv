<?php

namespace api\controllers;

use api\models\ProfileForm;
use api\models\ProfileImageForm;
use common\models\User;
use yii\db\Exception;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

class ProfileController extends Controller
{
    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => Cors::className(),
        ];

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBasicAuth::className(),
                HttpBearerAuth::className(),
                QueryParamAuth::className(),
            ],
        ];

        $behaviors['contentNegotiator']      = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        return User::dataUser();
    }

    public function actionUpdate()
    {
        $model = new ProfileForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->updateUser()) {
                return [
                    'message'   => 'Вы успешно обновили личные данные!',
                    'user'      => User::dataUser(),
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    public function actionUploadImage()
    {
        $model = new ProfileImageForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $user = $model->upload()) {
                return [
                    'message'   => 'Вы успешно обновили аватар!',
                    'data'      => $user,
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

    public function actionDeleteImage()
    {
        $model = new ProfileImageForm();

        try {
            if ($user = $model->delete()) {
                return [
                    'message'   => 'Вы успешно удалили аватар!',
                    'data'      => $user,
                ];

            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }

}