<?php

namespace api\controllers;

use common\models\Sales;
use yii\filters\ContentNegotiator;
use yii\rest\Controller;
use yii\web\Response;

class SalesController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        return [
            'contentNegotiator' => [
                'class' => ContentNegotiator::className(),
                'formats' => [
                    'application/json' => Response::FORMAT_JSON,
                ],
            ],
        ];
    }

    public function actionIndex()
    {
        return Sales::getAll();
    }

    public function actionView($id)
    {
        return Sales::getOne($id);
    }

}
