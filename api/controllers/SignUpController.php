<?php

namespace api\controllers;

use api\models\SignUpForm;
use yii\db\Exception;
use yii\filters\ContentNegotiator;
use yii\filters\Cors;
use yii\helpers\ArrayHelper;
use yii\rest\Controller;
use yii\web\HttpException;
use yii\web\Response;

class SignUpController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        parent::behaviors();

        $behaviors['corsFilter' ] = [
            'class' => Cors::className(),
        ];

        $behaviors['contentNegotiator']      = [
            'class' => ContentNegotiator::className(),
            'formats' => [
                'application/json' => Response::FORMAT_JSON,
            ],
        ];

        return $behaviors;
    }

    /**
     * Signs user up.
     *
     * @return mixed
     * @throws
     */
    public function actionIndex()
    {
        $model  = new SignUpForm();

        try {
            if ($model->load(\Yii::$app->request->post(), '') && $customer = $model->signup()) {
                $user   = $model->dataUser($customer);
                return [
                    'message'   => 'Благодарим Вас за регистрацию!',
                    'token'     => $customer->token,
                    'user'      => $user,
                ];
            } else {
                \Yii::$app->response->setStatusCode(422);
                $errors     = $model->firstErrors;
                foreach ($errors as $error){
                    return [
                        'message'   => $error
                    ];
                    break;
                }
            }
        }catch (\Exception $e){
            throw new HttpException(403, $e->getMessage());
        }
    }
}