<?php

namespace api\models;

use common\models\Products;
use yii\base\Model;
use yii\web\HttpException;

class BasketForm extends Model
{
    public $product;
    public $count;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product'       => 'Продукт',
            'count'         => 'Количество',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product', 'count'], 'required'],
            [['product', 'count'], 'integer'],
        ];
    }

    /**
     * Create favorite
     * @return mixed
     * @throws
     */
    public function create()
    {
        if ($this->validate()) {


            if($favorite = UserFavorites::findOne(['user_id' => $user->id, 'product_id' => $this->product])){
                return 'Данный товар Вы уже добавили в избранное!';
            }else {
                $favorite = new UserFavorites();
                $favorite->user_id = $user->id;
                $favorite->product_id = $this->product;
                $favorite->created_at = time();

                if ($favorite->save()) {
                    return 'Вы успешно добавили товар в избранное!';
                }
            }
        }

        return false;
    }

}