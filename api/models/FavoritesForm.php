<?php

namespace api\models;

use common\models\Customers;
use common\models\User;
use common\models\UserFavorites;
use yii\base\Model;
use yii\web\HttpException;

class FavoritesForm extends Model
{
    public $product;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product'       => 'Продукт',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product'], 'required'],
            [['product'], 'integer'],
        ];
    }

    /**
     * Create favorite
     * @return mixed
     * @throws
     */
    public function create()
    {
        $user       = $this->getUser();

        if ($this->validate()) {

            if($favorite = UserFavorites::findOne(['user_id' => $user->id, 'product_id' => $this->product])){
                return 'Данный товар Вы уже добавили в избранное!';
            }else {
                $favorite = new UserFavorites();
                $favorite->user_id = $user->id;
                $favorite->product_id = $this->product;
                $favorite->created_at = time();

                if ($favorite->save()) {
                    return 'Вы успешно добавили товар в избранное!';
                }
            }
        }

        return false;
    }

    /**
     * Delete favorite
     * @return mixed
     * @throws
     */
    public function delete()
    {
        $user       = $this->getUser();

        if ($this->validate()) {

            $favorite       = UserFavorites::findOne(['user_id' => $user->id, 'product_id' => $this->product]);

            if($favorite->delete()){
                return 'Вы успешно удалили товар из избранное!';
            }
        }

        return false;
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => \Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }
}