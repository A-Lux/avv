<?php

namespace api\models;

use common\models\User;
use yii\base\Model;

/**
 * Login form
 */
class LoginForm extends Model
{
    public $email;
    public $password;

    private $_user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'password'], 'required'],

            [['email'], 'email'],

            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'email'             => 'Электронная почта',
            'password'          => 'Пароль',
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user) {
                $this->addError($attribute, 'Неправильный логин.');
            }elseif(!$user->validatePassword($this->password)){
                $this->addError($attribute, 'Неправильный пароль.');
            }
        }
    }

    /**
     * @return mixed|null
     * @throws
     */
    public function auth()
    {
        if ($this->validate()) {
            $user = $this->getUser();
            $user->generateToken();
            $user->save();

            return $user;
        } else {
            return null;
        }
    }


    /**
     * Finds user by [[email]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByEmail($this->email);
        }

        return $this->_user;
    }
}
