<?php

namespace api\models;

use common\models\OrderedProducts;
use common\models\Orders;
use yii\base\Model;
use yii\helpers\Json;

class OrderForm extends Model
{
    public $username;
    public $status;
    public $country;
    public $city;
    public $address;
    public $email;
    public $phone;
    public $message;
    public $sum;
    public $statusPay;
    public $typePay;
    public $typeDelivery;

    public $orderProducts;
    public $deliveryMethod;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status'        => 'Статус',
            'username'      => 'ФИО',
            'country'       => 'Страна',
            'city'          => 'Город',
            'address'       => 'Адрес',
            'email'         => 'Электронная почта',
            'phone'         => 'Телефон',
            'message'       => 'Комментарий',
            'statusPay'     => 'Статус оплаты',
            'typePay'       => 'Тип оплаты',
            'typeDelivery'  => 'Тип доставки',
            'deliveryMethod'=> 'Метод доставки'
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'string', 'min' => 2, 'max' => 255],

            [['status'], 'integer'],

            [['country', 'city', 'address'], 'required'],
            [['country', 'city', 'address'], 'string', 'max' => 128],

            [['typePay', 'sum', 'typeDelivery', 'deliveryMethod'], 'required'],
            [['statusPay', 'typePay', 'sum', 'typeDelivery'], 'integer'],

            [['message'], 'string'],


        ];
    }

    /**
     * Create new order
     * @return mixed
     */
    public function create()
    {
        $this->orderProducts    = \Yii::$app->request->post()['orderProducts'];
        if ($this->validate()) {

            $order                      = new Orders();
            $order->order_id            = time();
            $order->user_id             = \Yii::$app->user->identity->id ? \Yii::$app->user->identity->id : null;
            $order->username            = $this->username;
            $order->sum                 = $this->sum;
            $order->status              = 1;
            $order->country             = $this->country;
            $order->city                = $this->city;
            $order->address             = $this->address;
            $order->email               = $this->email;
            $order->phone               = $this->phone;
            $order->message             = $this->message;
            $order->typePay             = $this->typePay;
            $order->typeDelivery        = $this->typeDelivery;
            $order->statusPay           = 0;
            $order->deliveryMethod      = $this->deliveryMethod;

            if($order->save()){
                foreach($this->orderProducts as $item){
                    $orderedProducts                = new OrderedProducts();
                    $orderedProducts->order_id      = $order->id;
                    $orderedProducts->product_id    = $item['product_id'];
                    $orderedProducts->count         = $item['count'];

                    $orderedProducts->save();
                }
            }

            return $order;
        }

        return false;
    }

}