<?php

namespace api\models;

use common\models\Customers;
use common\models\User;
use yii\base\Model;
use yii\web\HttpException;

class ProfileForm extends Model
{
    public $username;
    public $phone;
    public $email;
    public $birthday;

    private $_user;

    public $password;
    public $new_password;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'username'              => 'Имя',
            'phone'                 => 'Номер телефона',
            'email'                 => 'Электронная почта',
            'birthday'              => 'День рождения',
            'password'              => 'Старый пароль',
            'new_password'          => 'Новый пароль',
            'password_repeat'       => 'Подтверждение пароля',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['username', 'phone'], 'string', 'max' => 255],

            [['birthday'], 'string'],

        ];
    }

    /**
     * Update customer
     * @return mixed
     * @throws
     */
    public function updateUser()
    {
        $user       = $this->getUser();
        $client     = Customers::findOne(['user_id' => $user->id]);

        if(!$client){
            $client             = new Customers();
            $client->user_id    = $user->id;
        }

        if ($this->validate()) {

            $client->username       = $this->username;
            $user->phone            = preg_replace("/[^0-9]/", "", $this->phone);
            $client->birthday       = $this->birthday;

            if($client->save() && $user->save()){
                return $user;
            }
        }

        return false;
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();
            if (!$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Старый пароль не соответсвует.');
            }
        }
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => \Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }
}