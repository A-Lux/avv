<?php

namespace api\models;

use common\models\Customers;
use common\models\User;
use yii\base\Model;
use yii\web\HttpException;
use yii\web\UploadedFile;

class ProfileImageForm extends Model
{
    public $image;

    private $_user;

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'image'       => 'Изображение',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['image'], 'file', 'extensions' => 'png, jpg, jpeg, pdf, raw, gif, TIFF'],

        ];
    }

    /**
     * Upload customer
     * @return mixed
     * @throws
     */
    public function upload()
    {
        $user       = $this->getUser();

        if ($this->validate()) {
            $customer   = Customers::findOne(['user_id' => $user->id]);
            $oldImage   = $customer->image;
            $image = $this->uploadImage($customer, $oldImage);

            if($customer->save()){
                return \Yii::$app->request->hostInfo . $customer->getImage();
            }
        }

        return false;
    }

    /**
     * Delete image customer
     * @return mixed
     * @throws
     */
    public function delete()
    {
        $user       = $this->getUser();

        if ($this->validate()) {
            $customer   = Customers::findOne(['user_id' => $user->id]);
            $oldImage   = $customer->image;

            if (!empty($oldImage) && file_exists($customer->path() . $oldImage)) {
                unlink($customer->path() . $oldImage);
            }

            $customer->image    = null;

            if($customer->save()){
                return \Yii::$app->request->hostInfo . $customer->getImage();
            }
        }

        return false;
    }

    /**
     * Upload image user.
     * @param $model Customers
     * @param $oldImage
     * @throws
     * @return bool|mixed
     */
    protected function uploadImage($model, $oldImage)
    {
        $image  = UploadedFile::getInstanceByName('file');
        $hash   = \Yii::$app->security->generateRandomString();
 
        if($image != null){
            $time = time();
            $image->saveAs($model->path() . $time . '_' . $hash . '.' . $image->extension);
            $model->image = $time . '_' . $hash . '.' . $image->extension;

            if (!empty($oldImage) && file_exists($model->path() . $oldImage)) {
                unlink($model->path() . $oldImage);
            }

            return true;
        }else{
            $model->image = $oldImage;

            return false;
        }
    }

    /**
     * Finds user by [[user]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findOne(['id' => \Yii::$app->user->identity->id]);
        }

        return $this->_user;
    }
}