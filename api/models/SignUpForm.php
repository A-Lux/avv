<?php
namespace api\models;

use common\models\Customers;
use common\modules\user\models\BaseUser;
use Yii;
use yii\base\Model;
use common\models\User;

/**
 * SignUp form
 */
class SignUpForm extends Model
{
    public $username;
    public $phone;
    public $email;
    public $password;
    public $password_repeat;

    /**
     * Labels for fields
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'username'              => 'Имя',
            'phone'                 => 'Номер телефона',
            'email'                 => 'Электронная почта',
            'password'              => 'Пароль',
            'password_repeat'       => 'Подтверждение пароля',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User',
                'message' => 'Этот адрес электронной почты уже занят.'],

            ['username', 'string', 'max' => 255],
            ['username', 'required'],
            ['phone', 'required'],

            [['password', 'password_repeat'], 'required'],
            ['password', 'string', 'min' => 8],
            ['password_repeat', 'compare',
                'compareAttribute' => 'password',
                'message' => 'Пароли не совпадают'],
        ];
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function signUp()
    {
        if ($this->validate()) {
            $user           = new User();
            $user->email    = $this->email;
            $user->phone    = preg_replace("/[^0-9]/", "", $this->phone);
            $user->role     = BaseUser::ROLE_USER;
            $user->status   = BaseUser::STATUS_ACTIVE;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            $user->generateToken();

            if($user->save()){
                $client             = new Customers();
                $client->user_id    = $user->id;
                $client->username   = $this->username;
                $client->save();

                return $user;
            }

        }

        return false;
    }

    public function dataUser($model)
    {
        $customers  = Customers::findOne(['user_id' => $model->id]);
        return [
            'id'            => $model->id,
            'username'      => $customers->username,
            'token'         => $model->token,
            'phone'         => $model->phone,
            'email'         => $model->email,
            'birthday'      => $customers->birthday,
        ];
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => 'AVV robot'])
            ->setTo($this->email)
            ->setSubject('Вы зарегистрировались на сайте AVV')
            ->send();
    }
}
