<?php

namespace backend\controllers;

use Yii;
use common\models\About;
use backend\models\search\AboutSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * AboutController implements the CRUD actions for About model.
 */
class AboutController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = About::className();
            $this->searchModel = AboutSearch::className();

            return true;
        }

        return false;
    }
}
