<?php

namespace backend\controllers;

use Yii;
use common\models\BrandProducts;
use backend\models\search\BrandProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * BrandProductsController implements the CRUD actions for BrandProducts model.
 */
class BrandProductsController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createBrandProducts',
        'view'   => 'viewBrandProducts',
        'update' => 'updateBrandProducts',
        'index'  => 'indexBrandProducts',
        'delete' => 'deleteBrandProducts',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = BrandProducts::className();
            $this->searchModel = BrandProductsSearch::className();

            return true;
        }

        return false;
    }
}
