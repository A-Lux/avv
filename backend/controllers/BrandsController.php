<?php

namespace backend\controllers;

use common\models\BannerProduct;
use common\models\BrandProducts;
use Yii;
use common\models\Brands;
use backend\models\search\BrandsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;

/**
 * BrandsController implements the CRUD actions for Brands model.
 */
class BrandsController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createBanner',
        'view'   => 'viewBanner',
        'update' => 'updateBanner',
        'index'  => 'indexBanner',
        'delete' => 'deleteBanner',
    ];

    /**
     * @param \yii\base\Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Brands::className();
            $this->searchModel = BrandsSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Brands model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }
        $model = new Brands();
        $products       = isset(Yii::$app->request->post()['products'])
            ? Yii::$app->request->post()['products']
            : '';

        if ($model->load(Yii::$app->request->post())) {

            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));
            $this->createImage($model);
            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }

                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Brands model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }
        $model      = $this->findModel($id);
        $oldImage   = $model->image;

        $products       = isset(Yii::$app->request->post()['products'])
            ? Yii::$app->request->post()['products']
            : '';
        $brandProducts  = BrandProducts::getBannerProduct($model);

        if ($model->load(Yii::$app->request->post())) {
            $model->slug = $this->generateCyrillicToLatin(strip_tags($model->name));
            $this->updateImage($model, $oldImage);

            if($model->save()) {
                if(!empty($products)) {
                    $this->saveProduct($products, $model);
                }

                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('update', [
            'model'         => $model,
            'brandProducts'  => $brandProducts,
        ]);
    }

    protected function saveProduct($products, $model)
    {
        BrandProducts::deleteAll(['brand_id' => $model->id]);

        foreach ($products as $product){
            $brandProduct              = new BrandProducts();
            $brandProduct->brand_id    = $model->id;
            $brandProduct->product_id  = $product;
            $brandProduct->save();
        }
    }
}
