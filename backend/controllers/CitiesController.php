<?php

namespace backend\controllers;

use Yii;
use common\models\Cities;
use backend\models\search\CitiesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * CitiesController implements the CRUD actions for Cities model.
 */
class CitiesController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createCity',
        'view'   => 'viewCity',
        'update' => 'updateCity',
        'index'  => 'indexCity',
        'delete' => 'deleteCity',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Cities::className();
            $this->searchModel = CitiesSearch::className();

            return true;
        }

        return false;
    }
}
