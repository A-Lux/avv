<?php

namespace backend\controllers;

use Yii;
use common\models\Customers;
use backend\models\search\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createCustomers',
        'view'   => 'viewCustomers',
        'update' => 'updateCustomers',
        'index'  => 'indexCustomers',
        'delete' => 'deleteCustomers',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Customers::className();
            $this->searchModel = CustomersSearch::className();

            return true;
        }

        return false;
    }
}
