<?php

namespace backend\controllers;

use Yii;
use common\models\DeliveryAddress;
use backend\models\search\DeliveryAddressSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * DeliveryAddressController implements the CRUD actions for DeliveryAddress model.
 */
class DeliveryAddressController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = DeliveryAddress::className();
            $this->searchModel = DeliveryAddressSearch::className();

            return true;
        }

        return false;
    }
}
