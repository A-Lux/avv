<?php

namespace backend\controllers;

use Yii;
use common\models\FilterAttribute;
use backend\models\search\FilterAttributeSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * FilterAttributeController implements the CRUD actions for FilterAttribute model.
 */
class FilterAttributeController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createFilterAttr',
        'view'   => 'viewFilterAttr',
        'update' => 'updateFilterAttr',
        'index'  => 'indexFilterAttr',
        'delete' => 'deleteFilterAttr',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterAttribute::className();
            $this->searchModel = FilterAttributeSearch::className();

            return true;
        }

        return false;
    }
}
