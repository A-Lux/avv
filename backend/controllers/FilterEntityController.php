<?php

namespace backend\controllers;

use Yii;
use common\models\FilterEntity;
use backend\models\search\FilterEntitySearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * FilterEntityController implements the CRUD actions for FilterEntity model.
 */
class FilterEntityController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createFilterEntity',
        'view'   => 'viewFilterEntity',
        'update' => 'updateFilterEntity',
        'index'  => 'indexFilterEntity',
        'delete' => 'deleteFilterEntity',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterEntity::className();
            $this->searchModel = FilterEntitySearch::className();

            return true;
        }

        return false;
    }
}
