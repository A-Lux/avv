<?php

namespace backend\controllers;

use Yii;
use common\models\FilterValue;
use backend\models\search\FilterValueSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * FilterValueController implements the CRUD actions for FilterValue model.
 */
class FilterValueController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createFilterValue',
        'view'   => 'viewFilterValue',
        'update' => 'updateFilterValue',
        'index'  => 'indexFilterValue',
        'delete' => 'deleteFilterValue',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = FilterValue::className();
            $this->searchModel = FilterValueSearch::className();

            return true;
        }

        return false;
    }
}
