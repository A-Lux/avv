<?php

namespace backend\controllers;

use Yii;
use common\models\Orders;
use backend\models\search\OrdersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * OrdersController implements the CRUD actions for Orders model.
 */
class OrdersController extends BackendController
{
    /**
    * @var array
    */
    protected $permissions = [
        'create' => 'createAbout',
        'view'   => 'viewAbout',
        'update' => 'updateAbout',
        'index'  => 'indexAbout',
        'delete' => 'deleteAbout',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = Orders::className();
            $this->searchModel = OrdersSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Displays a single Orders model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        $model              = $this->findModel($id);
        $orderedProducts      = $model->orderedProducts;

        return $this->render('view', [
            'model'             => $model,
            'orderedProducts'   => $orderedProducts,
        ]);
    }

}
