<?php

namespace backend\controllers;

use Yii;
use common\models\ProductTypes;
use backend\models\search\ProductTypesSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * ProductTypesController implements the CRUD actions for ProductTypes model.
 */
class ProductTypesController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createProduct',
        'view'   => 'viewProduct',
        'update' => 'updateProduct',
        'index'  => 'indexProduct',
        'delete' => 'deleteProduct',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = ProductTypes::className();
            $this->searchModel = ProductTypesSearch::className();

            return true;
        }

        return false;
    }
}
