<?php

namespace backend\controllers;

use Yii;
use common\models\SalesBasket;
use backend\models\search\SalesBasketSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * ProductsController implements the CRUD actions for Products model.
 */
class SalesBasketController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createSalesBasket',
        'view'   => 'viewSalesBasket',
        'update' => 'updateSalesBasket',
        'index'  => 'indexSalesBasket',
        'delete' => 'deleteSalesBasket',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = SalesBasket::className();
            $this->searchModel = SalesBasketSearch::className();

            return true;
        }

        return false;
    }

    /**
     * Creates a new Products model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     * @throws ForbiddenHttpException
     */
    public function actionCreate()
    {
        if (false === \Yii::$app->user->can($this->permissions['create'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на создание');
        }
        $model = new SalesBasket();

        if ($model->load(Yii::$app->request->post())) {

            $model->slug    = $this->generateCyrillicToLatin(strip_tags($model->name));
            $this->createImage($model);

            if($model->save()) {
                \Yii::$app->session->setFlash('success', 'Объект успешно создан и сохранен!');
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Products model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     * @throws ForbiddenHttpException
     */
    public function actionUpdate($id)
    {
        if (!\Yii::$app->user->can($this->permissions['update'])) {
            throw new ForbiddenHttpException('У вас недостаточно прав на изменение');
        }
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {

            if($model->save()) {
                \Yii::$app->session->setFlash('info', 'Изменения приняты!');
                return $this->redirect(['index']);
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }
}
