<?php

namespace backend\controllers;

use Yii;
use common\models\TypeProducts;
use backend\models\search\TypeProductsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\ForbiddenHttpException;
use yii\base\Action;

/**
 * TypeProductsController implements the CRUD actions for TypeProducts model.
 */
class TypeProductsController extends BackendController
{
    /**
     * @var array
     */
    protected $permissions = [
        'create' => 'createProduct',
        'view'   => 'viewProduct',
        'update' => 'updateProduct',
        'index'  => 'indexProduct',
        'delete' => 'deleteProduct',
    ];

    /**
     * @param Action $action
     * @return bool
     * @throws
    */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            $this->model       = TypeProducts::className();
            $this->searchModel = TypeProductsSearch::className();

            return true;
        }

        return false;
    }
}
