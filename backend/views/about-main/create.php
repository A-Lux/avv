<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutMain */

$this->title = 'Создание Компания AVV';
$this->params['breadcrumbs'][] = ['label' => 'Компания AVV', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="about-main-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
