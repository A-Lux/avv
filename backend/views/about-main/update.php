<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\AboutMain */

$this->title = 'Редактировать Компания AVV: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Компания AVV', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="about-main-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
