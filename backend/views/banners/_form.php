<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use common\models\Banners;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="banners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload'            => false,
            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
            'initialPreviewAsData'  => true,
            'initialCaption'        => $model->isNewRecord ? '': $model->image,
            'showRemove'            => true,
            'deleteUrl'             => \yii\helpers\Url::to(['/banners/delete-image', 'id'=> $model->id]),
        ] ,
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?= $form->field($model, 'isButton')->dropDownList(Banners::buttonDescription()) ?>

    <?= $form->field($model, 'linkButton')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
