<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use common\models\Banners;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model common\models\Banners */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Баннеры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="banners-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
                'id',
            'title',
            'content:ntext',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>100]);
                }
            ],
            [
                'attribute' => 'isButton',
                'filter' => Banners::buttonDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(Banners::buttonDescription(), $model->isButton);
                },
                'format' => 'raw',
            ],
            'linkButton',
            'created_at',
    ],
    ]) ?>

</div>
