<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\BrandProducts */

$this->title = 'Создание Brand Products';
$this->params['breadcrumbs'][] = ['label' => 'Brand Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="brand-products-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
