<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;
use common\models\Products;
use common\models\Brands;

/* @var $this yii\web\View */
/* @var $model common\models\Brands */
/* @var $form yii\widgets\ActiveForm */
/* @var $brandProducts[] common\models\BrandProducts */

$products   = Products::getAll();
?>

<div class="brands-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true" class="nav-link active">Общие</a>
            </li>
            <li class="nav-item">
                <a id="product-tab" data-toggle="tab" href="#product" role="tab" aria-controls="product" aria-selected="false" class="nav-link">Продукты</a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="product" role="tabpanel" aria-labelledby="product-tab" class="tab-pane fade">

                <div class="form-group">
                    <label> Продукты для бренда</label>
                    <select class="form-control products-brand" multiple="multiple"
                            data-placeholder="Выберите товар" style="width: 100%;" name="products[]">
                        <? foreach ($products as $product): ?>
                            <option value="<?= $product->id; ?>"
                                <?= $model->isNewRecord
                                    ? ''
                                    : in_array($product->id, $brandProducts) ? 'selected' : ''?>
                            >
                                <?= $product->name; ?>
                            </option>
                        <? endforeach; ?>
                    </select>
                </div>

            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'isMain')->dropDownList(Brands::mainDescription()) ?>

                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',    // basic, standard, full
                        'inline' => false,      //по умолчанию false
                    ])
                ]); ?>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true,
                        'deleteUrl'             => \yii\helpers\Url::to(['/brands/delete-image', 'id'=> $model->id]),
                    ] ,
                    'options' => ['accept' => 'image/*'],
                ]); ?>

            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
