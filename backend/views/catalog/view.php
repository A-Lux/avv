<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Catalog;

/* @var $this yii\web\View */
/* @var $model common\models\Catalog */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Каталог продуктов', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="catalog-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        [
            'attribute' => 'parent_id',
            'value' => function ($model) {
                return $model->parent
                    ? Html::a($model->parent->name, ['view', 'id' => $model->parent->id])
                    : '';
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'status',
            'value' => function ($model) {
                return ArrayHelper::getValue(Catalog::statusDescription(), $model->status);
            },
            'format' => 'raw',
        ],
        'name',
        'slug:ntext',
        'content:ntext',
        [
            'format' => 'html',
            'attribute' => 'image',
            'value' => function($model){
                return Html::img($model->getImage(), ['width'=>100]);
            }
        ],
        'metaName',
        'metaDesc:ntext',
        'metaKey:ntext',
        'created_at',
    ],
    ]) ?>

</div>
