<?php

use yii\helpers\Html;
use yii\grid\GridView;
use common\models\Cities;
use common\models\DeliveryAddress;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\DeliveryAddressSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Адреса доставок';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="delivery-address-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'city_id',
                'filter'    => Cities::getList(),
                'value' => function ($model) {
                    return
                        Html::a($model->city->name, ['/cities/view', 'id' => $model->city->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => DeliveryAddress::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(DeliveryAddress::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'address',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
