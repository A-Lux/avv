<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\DeliveryAddress;

/* @var $this yii\web\View */
/* @var $model common\models\DeliveryAddress */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Адреса доставок', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="delivery-address-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'city_id',
                'value' => function ($model) {
                    return
                        Html::a($model->city->name, ['/cities/view', 'id' => $model->city->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'filter' => DeliveryAddress::statusDescription(),
                'value' => function ($model) {
                    return ArrayHelper::getValue(DeliveryAddress::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'address',
            'created_at',
        ],
    ]) ?>

</div>
