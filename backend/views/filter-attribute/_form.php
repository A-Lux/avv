<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FilterEntity;

/* @var $this yii\web\View */
/* @var $model common\models\FilterAttribute */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filter-attribute-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'entity_id')->dropDownList(FilterEntity::getList()) ?>

    <?= $form->field($model, 'name')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
