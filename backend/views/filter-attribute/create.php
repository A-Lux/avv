<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterAttribute */

$this->title = 'Создание Аттрибуты фильтров';
$this->params['breadcrumbs'][] = ['label' => 'Аттрибуты фильтров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-attribute-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
