<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FilterAttributeSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Аттрибуты фильтров';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-attribute-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'entity_id',
                'value' => function ($model) {
                    return
                        Html::a($model->entity->name,
                            ['/filter-entity/view', 'id' => $model->entity->id]);
                },
                'format' => 'raw',
            ],
            'name:ntext',
            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
