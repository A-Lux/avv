<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FilterAttribute */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Аттрибуты фильтров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="filter-attribute-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'entity_id',
                'value' => function ($model) {
                    return
                        Html::a($model->entity->name,
                            ['/filter-entity/view', 'id' => $model->entity->id]);
                },
                'format' => 'raw',
            ],
            'name:ntext',
            'created_at',
        ],
    ]) ?>

</div>
