<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterEntity */

$this->title = 'Создание Значение фильтров';
$this->params['breadcrumbs'][] = ['label' => 'Значение фильтров', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-entity-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
