<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterEntity */

$this->title = 'Редактировать Значение фильтров: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Значение фильтров', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="filter-entity-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
