<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\FilterAttribute;
use common\models\Products;

/* @var $this yii\web\View */
/* @var $model common\models\FilterValue */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="filter-value-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'attribute_id')->dropDownList(FilterAttribute::getList()) ?>

    <?= $form->field($model, 'product_id')->dropDownList(Products::getList()) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
