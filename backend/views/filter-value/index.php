<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\search\FilterValueSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Продукты/Фильтры';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="filter-value-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Создать', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            [
                'attribute' => 'attribute_id',
                'value' => function ($model) {
                    return
                        Html::a($model->attribute0->name,
                            ['/filter-attribute/view', 'id' => $model->attribute0->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/products/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],

            'created_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
