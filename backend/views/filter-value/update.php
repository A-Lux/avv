<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\FilterValue */

$this->title = 'Редактировать Продукты/Фильтры: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Продукты/Фильтры', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="filter-value-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
