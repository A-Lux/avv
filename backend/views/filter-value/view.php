<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\FilterValue */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Продукты/Фильтры', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="filter-value-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'attribute_id',
                'value' => function ($model) {
                    return
                        Html::a($model->attribute0->name,
                            ['/filter-attribute/view', 'id' => $model->attribute0->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'product_id',
                'value' => function ($model) {
                    return
                        Html::a($model->product->name, ['/products/view', 'id' => $model->product->id]);
                },
                'format' => 'raw',
            ],
            'created_at',
        ],
    ]) ?>

</div>
