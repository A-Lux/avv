<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p>Alexander Pierce</p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'По сайту', 'options' => ['class' => 'header']],
                    ['label' => 'Меню', 'icon' => 'bookmark-o', 'url' => ['/menu']],

                    [
                        'label' => 'Продукция',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Каталог', 'icon' => 'bookmark-o', 'url' => ['/catalog']],
                            ['label' => 'Товары', 'icon' => 'bookmark-o', 'url' => ['/products']],
                            ['label' => 'Изображении продуктов', 'icon' => 'ship',
                                'url' => ['/product-images']],
                        ],
                    ],
                    [
                        'label' => 'Фильтры',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Значение фильтров', 'icon' => 'ship', 'url' => ['/filter-entity']],
                            ['label' => 'Аттрибуты фильтров', 'icon' => 'ship', 'url' => ['/filter-attribute']],
                            ['label' => 'Продукты/Фильтры', 'icon' => 'ship', 'url' => ['/filter-value']],
                        ],
                    ],
                    [
                        'label' => 'Типы товаров',
                        'icon' => 'book',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Тип', 'icon' => 'ship', 'url' => ['/product-types']],
                            ['label' => 'Продукты типов', 'icon' => 'ship', 'url' => ['/type-products']],
                        ],
                    ],

                    ['label' => 'Заказы', 'icon' => 'ship', 'url' => ['/orders']],
                    ['label' => 'О компании', 'icon' => 'puzzle-piece', 'url' => ['/about']],
                    ['label' => 'Компания AVV', 'icon' => 'puzzle-piece', 'url' => ['/about-main']],
                    ['label' => 'Контакты', 'icon' => 'puzzle-piece', 'url' => ['/contact']],
                    ['label' => 'Бренды', 'icon' => 'handshake-o', 'url' => ['/brands']],
                    ['label' => 'Акции и скидки', 'icon' => 'handshake-o', 'url' => ['/sales']],
                ],
            ]
        ) ?>

        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget' => 'tree'],
                'items' => [
                    ['label' => 'Основное', 'options' => ['class' => 'header']],
//                    ['label' => 'Переводы', 'icon' => 'language', 'url' => ['/source-message/']],
//                    ['label' => 'Пользователи', 'icon' => 'user-circle-o', 'url' => ['/users']],
                    ['label' => 'Баннер', 'icon' => 'columns', 'url' => ['/banners']],
                    ['label' => 'Города доставок', 'icon' => 'handshake-o', 'url' => ['/cities']],
                    ['label' => 'Адреса доставок', 'icon' => 'handshake-o', 'url' => ['/delivery-address']],
                    ['label' => 'Логотип', 'icon' => 'image', 'url' => ['/logo']],
                    ['label' => 'Документы', 'icon' => 'image', 'url' => ['/documents']],
                    ['label' => 'Элекронная почта', 'icon' => 'ship', 'url' => ['/email-for-request']],
                    ['label' => 'Скидки', 'icon' => 'ship', 'url' => ['/sales-basket']],

                ],
            ]
        ) ?>

    </section>

</aside>
