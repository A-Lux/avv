<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Menu;

/* @var $this yii\web\View */
/* @var $model common\models\Menu */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Меню', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="menu-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'name',
        'url:url',
        'key',
        [
            'attribute' => 'isHeader',
            'filter' => Menu::headerDescription(),
            'value' => function ($model) {
                return ArrayHelper::getValue(Menu::headerDescription(), $model->isHeader);
            },
            'format' => 'raw',
        ],
        [
            'attribute' => 'isFooter',
            'filter' => Menu::footerDescription(),
            'value' => function ($model) {
                return ArrayHelper::getValue(Menu::footerDescription(), $model->isFooter);
            },
            'format' => 'raw',
        ],
        'metaName',
        'metaDesc:ntext',
        'metaKey:ntext',
        'created_at',
    ],
    ]) ?>

</div>
