<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Products;
use common\models\Catalog;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

    <?php $form = ActiveForm::begin(); ?>

    <div class="col-md-12 pl-0 pr-0">
        <div class="form-group" style="float: right;margin-top:7px;">
            <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
        </div>
        <ul id="myTab" role="tablist" class="nav nav-tabs">
            <li class="nav-item active">
                <a id="home-tab" data-toggle="tab" href="#home" role="tab"
                   aria-controls="home" aria-selected="true" class="nav-link active">
                    Общие
                </a>
            </li>
            <li class="nav-item">
                <a id="addition-tab" data-toggle="tab" href="#addition" role="tab"
                   aria-controls="addition" aria-selected="false" class="nav-link">
                    Дополнительно
                </a>
            </li>
        </ul>
        <div id="myTabContent" class="tab-content bg-white box-shadow p-4 mb-4">
            <div id="addition" role="tabpanel" aria-labelledby="addition-tab"
                 class="tab-pane fade">

                <?= $form->field($model, 'content')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',    // basic, standard, full
                        'inline' => false,      //по умолчанию false
                    ])
                ]); ?>

                <?= $form->field($model, 'characteristic')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',    // basic, standard, full
                        'inline' => false,      //по умолчанию false
                    ])
                ]); ?>

                <?= $form->field($model, 'manufacturer')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'production')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'star')->textInput() ?>

                <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

                <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>

            </div>

            <div id="home" role="tabpanel" aria-labelledby="home-tab" class="tab-pane fade show active in">

                <?= $form->field($model, 'category_id')->dropDownList(Catalog::getParentList()) ?>

                <?= $form->field($model, 'status')->dropDownList(Products::statusDescription()) ?>

                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>

                <?= $form->field($model, 'price')->textInput() ?>

                <?= $form->field($model, 'in_stock')->dropDownList(Products::stockDescription()) ?>

                <?= $form->field($model, 'in_weight')->dropDownList(Products::weightDescription()) ?>

                <?= $form->field($model, 'weight')->textInput() ?>

                <?= $form->field($model, 'size')->textInput() ?>

                <?= $form->field($model, 'image')->widget(FileInput::classname(), [
                    'pluginOptions' => [
                        'showUpload'            => false ,
                        'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
                        'initialPreviewAsData'  => true,
                        'initialCaption'        => $model->isNewRecord ? '': $model->image,
                        'showRemove'            => true ,
                        'deleteUrl'             => \yii\helpers\Url::to(['/products/delete-image', 'id'=> $model->id]),
                    ] ,'options' => ['accept' => 'image/*'],
                ]); ?>

                <?= $form->field($model, 'description')->widget(CKEditor::className(), [
                    'editorOptions' => ElFinder::ckeditorOptions('elfinder', [
                        'preset' => 'full',    // basic, standard, full
                        'inline' => false,      //по умолчанию false
                    ])
                ]); ?>

            </div>

        </div>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
