<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\SaleBasket;
use kartik\file\FileInput;
use mihaildev\ckeditor\CKEditor;
use mihaildev\elfinder\ElFinder;

/* @var $this yii\web\View */
/* @var $model common\models\Products */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="products-form">

        <div class="contact-form">

            <?php $form = ActiveForm::begin(); ?>


            <?= $form->field($model, 'discount')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'sum')->textInput(['maxlength' => true]) ?>



            <div class="form-group">
                <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
            </div>

            <?php ActiveForm::end(); ?>

        </div>


    </div>
