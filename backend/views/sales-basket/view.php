<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\helpers\ArrayHelper;
use common\models\Products;

/* @var $this yii\web\View */
/* @var $model common\models\Products */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Продукты', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'category_id',
                'value' => function ($model) {
                    return
                        Html::a($model->category->name, ['/catalog/view', 'id' => $model->category->id]);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'status',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Products::statusDescription(), $model->status);
                },
                'format' => 'raw',
            ],
            'name',
            'code',
            'price',
            [
                'format' => 'html',
                'attribute' => 'image',
                'value' => function($model){
                    return Html::img($model->getImage(), ['width'=>150]);
                }
            ],
            'slug:ntext',
            'description:ntext',
            'content:ntext',
            'characteristic:ntext',
            'manufacturer',
            'production',
            'star',
            [
                'attribute' => 'in_stock',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Products::stockDescription(), $model->in_stock);
                },
                'format' => 'raw',
            ],
            [
                'attribute' => 'in_weight',
                'value' => function ($model) {
                    return ArrayHelper::getValue(Products::weightDescription(), $model->in_weight);
                },
                'format' => 'raw',
            ],
            'weight',
            'size',
            'metaName',
            'metaDesc:ntext',
            'metaKey:ntext',
            'created_at',
        ],
    ]) ?>

</div>
