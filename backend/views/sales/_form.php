<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\file\FileInput;

/* @var $this yii\web\View */
/* @var $model common\models\Sales */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="sales-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'image')->widget(FileInput::classname(), [
        'pluginOptions' => [
            'showUpload'            => false,
            'initialPreview'        => $model->isNewRecord ? '' : $model->getImage(),
            'initialPreviewAsData'  => true,
            'initialCaption'        => $model->isNewRecord ? '': $model->image,
            'showRemove'            => true,
            'deleteUrl'             => \yii\helpers\Url::to(['/sales/delete-image', 'id'=> $model->id]),
        ] ,
        'options' => ['accept' => 'image/*'],
    ]); ?>

    <?= $form->field($model, 'metaName')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'metaDesc')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'metaKey')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
