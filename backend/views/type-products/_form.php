<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\Products;
use common\models\ProductTypes;

/* @var $this yii\web\View */
/* @var $model common\models\TypeProducts */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="type-products-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'type_id')->dropDownList(ProductTypes::getList()) ?>

    <?= $form->field($model, 'product_id')->dropDownList(Products::getList()) ?>

<!--    --><?//= $form->field($model, 'created_at')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
