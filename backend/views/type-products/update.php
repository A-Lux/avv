<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\TypeProducts */

$this->title = 'Редактировать Type Products: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Type Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Редактирование';
?>
<div class="type-products-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
