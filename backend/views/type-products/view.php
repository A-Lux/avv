<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\TypeProducts */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Type Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="type-products-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Редактировать', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Удалить', ['delete', 'id' => $model->id], [
        'class' => 'btn btn-danger',
        'data' => [
        'confirm' => 'Вы действительно хотите удалить этот элемент?',
        'method' => 'post',
        ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
                    'id',
                    [
                        'attribute' => 'type_id',
                        'value' => function ($model) {
                            return
                                Html::a($model->type->name, ['/product-types/view', 'id' => $model->type->id]);
                        },
                        'format' => 'raw',
                    ],
                    [
                        'attribute' => 'product_id',
                        'value' => function ($model) {
                            return
                                Html::a($model->product->name, ['/products/view', 'id' => $model->product->id]);
                        },
                        'format' => 'raw',
                    ],
                'created_at',
        ],
    ]) ?>

</div>
