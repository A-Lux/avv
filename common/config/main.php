<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'authManager' => [
            'class'          => yii\rbac\PhpManager::className(),
            /** TODO: set default roles */
            'defaultRoles'   => array_values(\common\modules\user\models\BaseUser::roles()),
            'itemFile'       => '@common/rbac/_items.php',
            'assignmentFile' => '@common/rbac/_assignments.php',
            'ruleFile'       => '@common/rbac/_rules.php',
        ],
        'formatter'    => [
            'class'           => yii\i18n\Formatter::className(),
            'defaultTimeZone' => 'Asia/Almaty',
            'timeZone'        => 'Asia/Almaty',
        ],
        'user'         => [
            'identityClass'   => common\modules\user\models\User::className(),
            'loginUrl'        => ['/user/identity/login'],
            'enableAutoLogin' => true,
            'identityCookie'  => [
                'name'     => '_identity',
                'httpOnly' => true,
            ],
        ],
    ],
    'modules'         => [
        'user' => [
            'class' => common\modules\user\Module::className(),
        ],
    ],
];
