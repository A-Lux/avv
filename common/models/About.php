<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about".
 *
 * @property int $id
 * @property int|null $status
 * @property string $title
 * @property string|null $content
 * @property string|null $image
 */
class About extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status'], 'integer'],
            [['title'], 'required'],
            [['content', 'image'], 'string'],
            [['title'], 'string', 'max' => 128],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'status'        => 'Статус',
            'title'         => 'Заголовок',
            'content'       => 'Контент',
            'image'         => 'Изображение',
        ];
    }

    public static function getOne()
    {
        return static::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
