<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "about_main".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $image
 * @property string|null $text
 */
class AboutMain extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'about_main';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'image', 'text'], 'string'],
            [['title'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'title'         => 'Заголовок',
            'content'       => 'Контент',
            'image'         => 'Изображение',
            'text'          => 'Текст',
        ];
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/about-main/';
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/about-main/' . $this->image : '';
    }

    public static function getOne()
    {
        return static::find()->orderBy(['id' => SORT_ASC])->one();
    }
}
