<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "banners".
 *
 * @property int $id
 * @property string|null $title
 * @property string|null $content
 * @property string|null $image
 * @property int|null $isButton
 * @property string|null $linkButton
 * @property int $sort
 * @property string|null $created_at
 */
class Banners extends \yii\db\ActiveRecord
{

    const BUTTON_DISABLED       = 0;
    const BUTTON_ACTIVE         = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'banners';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['content', 'image'], 'string'],
            [['isButton', 'sort'], 'integer'],
            [['title'], 'required'],
            [['created_at'], 'safe'],
            [['title', 'linkButton'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'title'             => 'Заголовок',
            'content'           => 'Содержание',
            'image'             => 'Изображение',
            'isButton'          => 'Кнопка',
            'linkButton'        => 'Ссылка',
            'sort'              => 'Сортировка',
            'created_at'        => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $model = self::find()->orderBy(['sort' => SORT_DESC])->one();
            if (!$model) {
                $this->sort = 1;
            } elseif ($this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * @return array
     */
    public static function buttons()
    {
        return [
            self::BUTTON_DISABLED   => 'disabled',
            self::BUTTON_ACTIVE     => 'active',
        ];
    }

    /**
     * @return array
     */
    public static function buttonDescription()
    {
        return [
            self::BUTTON_DISABLED   => 'закрытый',
            self::BUTTON_ACTIVE     => 'активный',
        ];
    }

    public function path()
    {
        return Yii::getAlias('@backend') . '/web/uploads/images/banners/';
    }

    public function getImage()
    {
        return ($this->image) ? '/uploads/images/banners/' . $this->image : '';
    }

    public static function getBanners()
    {
        return static::find()->all();
    }
}
