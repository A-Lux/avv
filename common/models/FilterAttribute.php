<?php

namespace common\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "filter_attribute".
 *
 * @property int $id
 * @property int $entity_id
 * @property string|null $name
 * @property int|null $sort
 * @property string|null $created_at
 *
 * @property FilterEntity $entity
 * @property FilterValue[] $filterValues
 */
class FilterAttribute extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filter_attribute';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['entity_id'], 'required'],
            [['entity_id', 'sort'], 'integer'],
            [['name'], 'string'],
            [['created_at'], 'safe'],
            [['entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilterEntity::className(), 'targetAttribute' => ['entity_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'ID',
            'entity_id'     => 'Значение фильтров',
            'name'          => 'Наименование',
            'sort'          => 'Сортировка',
            'created_at'    => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $model = self::find()->orderBy(['sort' => SORT_DESC])->one();
            if (!$model) {
                $this->sort = 1;
            } elseif ($this->id != $model->id) {
                $this->sort = $model->sort + 1;
            }
            $this->created_at    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[Entity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(FilterEntity::className(), ['id' => 'entity_id']);
    }

    /**
     * Gets query for [[FilterValues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFilterValues()
    {
        return $this->hasMany(FilterValue::className(), ['attribute_id' => 'id']);
    }

    public static function getList()
    {
        return ArrayHelper::map(self::find()->all(), 'id', 'name');
    }
}
