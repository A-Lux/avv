<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "filter_value".
 *
 * @property int $id
 * @property int $attribute_id
 * @property int $product_id
 * @property string|null $created_at
 *
 * @property FilterAttribute $attribute0
 * @property Products $product
 */
class FilterValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'filter_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['attribute_id', 'product_id'], 'required'],
            [['attribute_id', 'product_id'], 'integer'],
            [['created_at'], 'safe'],
            [['attribute_id'], 'exist', 'skipOnError' => true, 'targetClass' => FilterAttribute::className(), 'targetAttribute' => ['attribute_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Products::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id'                => 'ID',
            'attribute_id'      => 'Атрибут фильтров',
            'product_id'        => 'Продукт',
            'created_at'        => 'Дата создания',
        ];
    }

    public function beforeSave($insert)
    {
        if($this->isNewRecord){
            $this->created_at    = Yii::$app->formatter->asDate(time(), 'Y-MM-dd H:i');
        }
        return parent::beforeSave($insert); // TODO: Change the autogenerated stub
    }

    /**
     * Gets query for [[Attribute0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAttribute0()
    {
        return $this->hasOne(FilterAttribute::className(), ['id' => 'attribute_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Products::className(), ['id' => 'product_id']);
    }
}
