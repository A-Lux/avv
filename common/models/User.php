<?php
namespace common\models;

use common\modules\user\models\BaseUser;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string  $email
 * @property string  $phone
 * @property string  $auth_key
 * @property string  $password_hash
 * @property string  $token
 * @property integer $status
 * @property integer $role
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 *
 * @property Customers $customers
 */
class User extends \common\modules\user\models\User
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * Gets query for [[Customers]].
     * @return \yii\db\ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(Customers::className(), ['user_id' => 'id']);
    }

    public static function getOne()
    {
        return self::findOne(['id' => Yii::$app->user->identity->id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function dataUser()
    {
        $user       = static::findOne(['id' => Yii::$app->user->identity->id]);
        $customer   = Customers::findOne(['user_id' => $user->id]);

        return [
            'id'            => $user->id,
            'username'      => $customer ? $customer->username : '',
            'token'         => $user->token,
            'phone'         => $user->phone,
            'email'         => $user->email,
            'birthday'      => $customer ? $customer->birthday : '',
            'image'         => $customer ? \Yii::$app->request->hostInfo . $customer->getImage() : '',
        ];
    }
}
