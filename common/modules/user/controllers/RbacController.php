<?php

    namespace common\modules\user\controllers;

    use common\modules\user\models\BaseUser;
    use common\modules\user\rbac\ForeignAccessRule;
    use common\modules\user\rbac\OwnerAccessRule;
    use common\modules\user\rbac\UserRoleRule;
    use yii\console\Controller;
    use yii\rbac\ManagerInterface;

    /**
     * Class RbacController
     * @package papalapa\yiistart\controllers
     */
    class RbacController extends Controller
    {
        /**
         * Permission to access to owner content
         * @var
         */
        public $ownerPermission;
        /**
         * Permission to access to foreign content
         * @var
         */
        public $foreignPermission;
        /**
         * @var ManagerInterface
         */
        protected $authManager;
        /**
         * @var array
         */
        protected $roles = [];

        /**
         * @inheritdoc
         */
        public function actionInit()
        {
            $this->authManager = \Yii::$app->authManager;
            $this->authManager->removeAll();
            echo 'Old rules has been removed.'.PHP_EOL;

            $this->createRoles();
            $this->createOwnerAccessRule();
            $this->createForeignAccessRule();
            $this->loadPermissions();
        }

        /**
         * Add rule which checks user foreign rights
         */
        protected function createForeignAccessRule()
        {
            $foreignAccessRule = new ForeignAccessRule();
            $this->authManager->add($foreignAccessRule);
            echo 'foreignAccess rule has been created.'.PHP_EOL;

            $foreignAccess = $this->authManager->createPermission('foreignAccess');
            echo 'Foreign permission has been created.'.PHP_EOL;

            $foreignAccess->description = 'Доступ к чужому контенту';
            $foreignAccess->ruleName    = $foreignAccessRule->name;
            echo 'Foreign permission attributes has been saved.'.PHP_EOL;

            $this->authManager->add($foreignAccess);
            echo 'Foreign permission has been added.'.PHP_EOL;

            $this->foreignPermission = $foreignAccess;
        }

        /**
         * Add rule which checks user owner rights
         */
        protected function createOwnerAccessRule()
        {
            $ownerAccessRule = new OwnerAccessRule();
            $this->authManager->add($ownerAccessRule);
            echo 'ownerAccess rule has been created.'.PHP_EOL;

            $ownerAccess = $this->authManager->createPermission('ownerAccess');
            echo 'Owner permission has been created.'.PHP_EOL;

            $ownerAccess->description = 'Доступ к своему контенту';
            $ownerAccess->ruleName    = $ownerAccessRule->name;
            echo 'Owner permission attributes has been saved.'.PHP_EOL;

            $this->authManager->add($ownerAccess);
            echo 'Owner permission has been added.'.PHP_EOL;

            $this->ownerPermission = $ownerAccess;
        }

        /**
         * Generating user roles
         */
        protected function createRoles()
        {
            $userRoleRule = new UserRoleRule();
            $this->authManager->add($userRoleRule);

            $this->roles[BaseUser::ROLE_GUEST]     = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_GUEST]);
            $this->roles[BaseUser::ROLE_USER]      = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_USER]);
            $this->roles[BaseUser::ROLE_OPERATOR]  = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_OPERATOR]);
            $this->roles[BaseUser::ROLE_MANAGER]   = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_MANAGER]);
            $this->roles[BaseUser::ROLE_ADMIN]     = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_ADMIN]);
            $this->roles[BaseUser::ROLE_DEVELOPER] = $this->authManager->createRole(BaseUser::roles()[BaseUser::ROLE_DEVELOPER]);
            echo 'User roles has been created.'.PHP_EOL;

            $this->roles[BaseUser::ROLE_GUEST]->ruleName     = $userRoleRule->name;
            $this->roles[BaseUser::ROLE_USER]->ruleName      = $userRoleRule->name;
            $this->roles[BaseUser::ROLE_OPERATOR]->ruleName  = $userRoleRule->name;
            $this->roles[BaseUser::ROLE_MANAGER]->ruleName   = $userRoleRule->name;
            $this->roles[BaseUser::ROLE_ADMIN]->ruleName     = $userRoleRule->name;
            $this->roles[BaseUser::ROLE_DEVELOPER]->ruleName = $userRoleRule->name;

            $this->authManager->add($this->roles[BaseUser::ROLE_GUEST]);
            $this->authManager->add($this->roles[BaseUser::ROLE_USER]);
            $this->authManager->add($this->roles[BaseUser::ROLE_OPERATOR]);
            $this->authManager->add($this->roles[BaseUser::ROLE_MANAGER]);
            $this->authManager->add($this->roles[BaseUser::ROLE_ADMIN]);
            $this->authManager->add($this->roles[BaseUser::ROLE_DEVELOPER]);
            echo 'User roles has been added.'.PHP_EOL;
        }

        /**
         * Making rules to control content
         * There are five default permissions:
         * - create content
         * - view content (with attribute is_active = false)
         * - listing content (include models with attribute is_active = false)
         * - update content
         * - delete content
         */
        protected function loadPermissions()
        {
            /** Управление пользователями */
            $createUser              = $this->authManager->createPermission('createUser');
            $createUser->description = 'Создание пользователей';
            $viewUser                = $this->authManager->createPermission('viewUser');
            $viewUser->description   = 'Просмотр пользователей';
            $indexUser               = $this->authManager->createPermission('indexUser');
            $indexUser->description  = 'Листинг пользователей';
            $updateUser              = $this->authManager->createPermission('updateUser');
            $updateUser->description = 'Изменение пользователей';
            $deleteUser              = $this->authManager->createPermission('deleteUser');
            $deleteUser->description = 'Удаление пользователей';

            /** Управление информациях о клиенте */
            $createClient              = $this->authManager->createPermission('createClient');
            $createClient->description = 'Создание информациях о клиенте';
            $viewClient                = $this->authManager->createPermission('viewClient');
            $viewClient->description   = 'Просмотр информациях о клиенте';
            $indexClient               = $this->authManager->createPermission('indexClient');
            $indexClient->description  = 'Листинг информациях о клиенте';
            $updateClient              = $this->authManager->createPermission('updateClient');
            $updateClient->description = 'Изменение информациях о клиенте';
            $deleteClient              = $this->authManager->createPermission('deleteClient');
            $deleteClient->description = 'Удаление информациях о клиенте';

            /** Управление переводами */
            $createTranslation              = $this->authManager->createPermission('createTranslation');
            $createTranslation->description = 'Создание переводов';
            $viewTranslation                = $this->authManager->createPermission('viewTranslation');
            $viewTranslation->description   = 'Просмотр переводов';
            $indexTranslation               = $this->authManager->createPermission('indexTranslation');
            $indexTranslation->description  = 'Листинг переводов';
            $updateTranslation              = $this->authManager->createPermission('updateTranslation');
            $updateTranslation->description = 'Изменение переводов';
            $deleteTranslation              = $this->authManager->createPermission('deleteTranslation');
            $deleteTranslation->description = 'Удаление переводов';

            /** Управление описаниями категорий переводов */
            $createSourceMessageCategory              = $this->authManager->createPermission('createSourceMessageCategory');
            $createSourceMessageCategory->description = 'Создание описаний категорий переводов';
            $viewSourceMessageCategory                = $this->authManager->createPermission('viewSourceMessageCategory');
            $viewSourceMessageCategory->description   = 'Просмотр описаний категорий переводов';
            $indexSourceMessageCategory               = $this->authManager->createPermission('indexSourceMessageCategory');
            $indexSourceMessageCategory->description  = 'Листинг описаний категорий переводов';
            $updateSourceMessageCategory              = $this->authManager->createPermission('updateSourceMessageCategory');
            $updateSourceMessageCategory->description = 'Изменение описаний категорий переводов';
            $deleteSourceMessageCategory              = $this->authManager->createPermission('deleteSourceMessageCategory');
            $deleteSourceMessageCategory->description = 'Удаление описаний категорий переводов';

            /** Управление меню */
            $createMenu              = $this->authManager->createPermission('createMenu');
            $createMenu->description = 'Создание пунктов меню';
            $viewMenu                = $this->authManager->createPermission('viewMenu');
            $viewMenu->description   = 'Просмотр пунктов меню';
            $indexMenu               = $this->authManager->createPermission('indexMenu');
            $indexMenu->description  = 'Листинг пунктов меню';
            $updateMenu              = $this->authManager->createPermission('updateMenu');
            $updateMenu->description = 'Изменение пунктов меню';
            $deleteMenu              = $this->authManager->createPermission('deleteMenu');
            $deleteMenu->description = 'Удаление пунктов меню';

            /** Управление языковыми версиями */
            $createLanguage              = $this->authManager->createPermission('createLanguage');
            $createLanguage->description = 'Создание языка';
            $viewLanguage                = $this->authManager->createPermission('viewLanguage');
            $viewLanguage->description   = 'Просмотр языка';
            $indexLanguage               = $this->authManager->createPermission('indexLanguage');
            $indexLanguage->description  = 'Листинг языка';
            $updateLanguage              = $this->authManager->createPermission('updateLanguage');
            $updateLanguage->description = 'Изменение языка';
            $deleteLanguage              = $this->authManager->createPermission('deleteLanguage');
            $deleteLanguage->description = 'Удаление языка';

            /** Управление баннерами */
            $createBanner              = $this->authManager->createPermission('createBanner');
            $createBanner->description = 'Создание баннерами';
            $viewBanner                = $this->authManager->createPermission('viewBanner');
            $viewBanner->description   = 'Просмотр баннерами';
            $indexBanner               = $this->authManager->createPermission('indexBanner');
            $indexBanner->description  = 'Листинг баннерами';
            $updateBanner              = $this->authManager->createPermission('updateBanner');
            $updateBanner->description = 'Изменение баннерами';
            $deleteBanner              = $this->authManager->createPermission('deleteBanner');
            $deleteBanner->description = 'Удаление баннерами';

            $createSalesBasket              = $this->authManager->createPermission('createSalesBasket');
            $createSalesBasket->description = 'Создание createSalesBasket';
            $viewSalesBasket                = $this->authManager->createPermission('viewSalesBasket');
            $viewSalesBasket->description   = 'Просмотр viewSalesBasket';
            $indexSalesBasket               = $this->authManager->createPermission('indexSalesBasket');
            $indexSalesBasket->description  = 'Листинг indexSalesBasket';
            $updateSalesBasket              = $this->authManager->createPermission('updateSalesBasket');
            $updateSalesBasket->description = 'Изменение updateSalesBasket';
            $deleteSalesBasket              = $this->authManager->createPermission('deleteSalesBasket');
            $deleteSalesBasket->description = 'Удаление deleteSalesBasket';


            /** Управление логотип */
            $createLogo              = $this->authManager->createPermission('createLogo');
            $createLogo->description = 'Создание логотипа';
            $viewLogo                = $this->authManager->createPermission('viewLogo');
            $viewLogo->description   = 'Просмотр логотипа';
            $indexLogo               = $this->authManager->createPermission('indexLogo');
            $indexLogo->description  = 'Листинг логотипа';
            $updateLogo              = $this->authManager->createPermission('updateLogo');
            $updateLogo->description = 'Изменение логотипа';
            $deleteLogo              = $this->authManager->createPermission('deleteLogo');
            $deleteLogo->description = 'Удаление логотипа';

            /** Управление Города */
            $createCity              = $this->authManager->createPermission('createCity');
            $createCity->description = 'Создание города';
            $viewCity                = $this->authManager->createPermission('viewCity');
            $viewCity->description   = 'Просмотр города';
            $indexCity               = $this->authManager->createPermission('indexCity');
            $indexCity->description  = 'Листинг города';
            $updateCity              = $this->authManager->createPermission('updateCity');
            $updateCity->description = 'Изменение города';
            $deleteCity              = $this->authManager->createPermission('deleteCity');
            $deleteCity->description = 'Удаление города';

            echo 'New permissions has been created.'.PHP_EOL;

            // ------------------------------------------------------------------------------

            /** Регистрация правил */
            $this->authManager->add($createUser);
            $this->authManager->add($viewUser);
            $this->authManager->add($indexUser);
            $this->authManager->add($updateUser);
            $this->authManager->add($deleteUser);

            /** Регистрация правил */
            $this->authManager->add($createClient);
            $this->authManager->add($viewClient);
            $this->authManager->add($indexClient);
            $this->authManager->add($updateClient);
            $this->authManager->add($deleteClient);

            $this->authManager->add($createTranslation);
            $this->authManager->add($viewTranslation);
            $this->authManager->add($indexTranslation);
            $this->authManager->add($updateTranslation);
            $this->authManager->add($deleteTranslation);

            $this->authManager->add($createSourceMessageCategory);
            $this->authManager->add($viewSourceMessageCategory);
            $this->authManager->add($indexSourceMessageCategory);
            $this->authManager->add($updateSourceMessageCategory);
            $this->authManager->add($deleteSourceMessageCategory);

            $this->authManager->add($createMenu);
            $this->authManager->add($viewMenu);
            $this->authManager->add($indexMenu);
            $this->authManager->add($updateMenu);
            $this->authManager->add($deleteMenu);

            $this->authManager->add($createLanguage);
            $this->authManager->add($viewLanguage);
            $this->authManager->add($indexLanguage);
            $this->authManager->add($updateLanguage);
            $this->authManager->add($deleteLanguage);

            $this->authManager->add($createBanner);
            $this->authManager->add($viewBanner);
            $this->authManager->add($indexBanner);
            $this->authManager->add($updateBanner);
            $this->authManager->add($deleteBanner);

            $this->authManager->add($createSalesBasket);
            $this->authManager->add($viewSalesBasket);
            $this->authManager->add($indexSalesBasket);
            $this->authManager->add($updateSalesBasket);
            $this->authManager->add($deleteSalesBasket);

            $this->authManager->add($createLogo);
            $this->authManager->add($viewLogo);
            $this->authManager->add($indexLogo);
            $this->authManager->add($updateLogo);
            $this->authManager->add($deleteLogo);

            $this->authManager->add($createCity);
            $this->authManager->add($viewCity);
            $this->authManager->add($indexCity);
            $this->authManager->add($updateCity);
            $this->authManager->add($deleteCity);

            echo 'New permissions has been added.'.PHP_EOL;

            // ------------------------------------------------------------------------------

            /** Добавление правил */
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $this->ownerPermission);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $this->foreignPermission);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createUser);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewUser);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexUser);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateUser);
//            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteUser); // only for developer

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createClient);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewClient);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexClient);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateClient);
//            $this->authManager->addChild($this->roles[BaseClient::ROLE_ADMIN], $deleteClient); // only for developer

            // $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createTranslation); // translation creates automatic
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewTranslation);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexTranslation);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateTranslation);
//            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteTranslation); // only for developer

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createMenu);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewMenu);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexMenu);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateMenu);
//            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteMenu); // only for developer

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createLanguage);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewLanguage);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexLanguage);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateLanguage);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteLanguage);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createBanner);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewBanner);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexBanner);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateBanner);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteBanner);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createSalesBasket);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewSalesBasket);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexSalesBasket);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateSalesBasket);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteSalesBasket);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createLogo);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewLogo);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexLogo);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateLogo);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteLogo);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $createCity);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $viewCity);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $indexCity);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $updateCity);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_ADMIN], $deleteCity);

            /** Developer */

            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $deleteUser);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $deleteMenu);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $deleteTranslation);

            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $createSourceMessageCategory);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $viewSourceMessageCategory);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $indexSourceMessageCategory);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $updateSourceMessageCategory);
            $this->authManager->addChild($this->roles[BaseUser::ROLE_DEVELOPER], $deleteSourceMessageCategory);

            echo 'All child permissions has been added.'.PHP_EOL;

            echo 'RBAC generate complete.'.PHP_EOL;
        }
    }
