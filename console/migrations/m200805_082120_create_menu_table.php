<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%menu}}`.
 */
class m200805_082120_create_menu_table extends Migration
{
    public $table               = 'menu';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'name'              => $this->string(128)->null(),
            'url'               => $this->string(128)->null(),
            'key'               => $this->string(128)->null(),
            'isHeader'          => $this->smallInteger()->null()->defaultValue(0),
            'isFooter'          => $this->smallInteger()->null()->defaultValue(0),
            'sort'              => $this->integer()->notNull(),
            'metaName'          => $this->string(255)->null(),
            'metaDesc'          => $this->text()->null(),
            'metaKey'           => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
