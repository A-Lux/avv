<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%banner}}`.
 */
class m200805_082348_create_banner_table extends Migration
{
    public $table               = 'banners';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'title'             => $this->string(255)->null(),
            'content'           => $this->text()->null(),
            'image'             => $this->text()->null(),
            'isButton'          => $this->smallInteger()->null()->defaultValue(0),
            'linkButton'        => $this->string(255)->null(),
            'sort'              => $this->integer()->notNull(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
