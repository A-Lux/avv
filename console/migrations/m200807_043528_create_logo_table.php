<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%logo}}`.
 */
class m200807_043528_create_logo_table extends Migration
{
    public $table               = 'logo';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'position'          => $this->integer()->defaultValue(0)->null(),
            'image'             => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%logo}}');
    }
}
