<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%catalog}}`.
 */
class m200817_043458_create_catalog_table extends Migration
{
    public $table               = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'parent_id'         => $this->integer()->null(),
            'status'            => $this->integer()->defaultValue(1)->null(),
            'name'              => $this->string(255)->null(),
            'slug'              => $this->text()->null(),
            'content'           => $this->text()->null(),
            'image'             => $this->string(255)->null(),
            'sort'              => $this->integer()->null(),
            'metaName'          => $this->string(255)->null(),
            'metaDesc'          => $this->text()->null(),
            'metaKey'           => $this->text()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
