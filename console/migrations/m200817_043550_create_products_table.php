<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%products}}`.
 */
class m200817_043550_create_products_table extends Migration
{
    public $table               = 'products';
    public $catalogTable        = 'catalog';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                    => $this->primaryKey(),
            'category_id'           => $this->integer()->null(),
            'status'                => $this->integer()->defaultValue(1)->null(),
            'name'                  => $this->string(255)->null(),
            'code'                  => $this->string(255)->null(),
            'price'                 => $this->integer()->null(),
            'image'                 => $this->text()->null(),
            'slug'                  => $this->text()->null(),
            'description'           => $this->text()->null(),
            'content'               => $this->text()->null(),
            'characteristic'        => $this->text()->null(),
            'manufacturer'          => $this->string(255)->null(),
            'production'            => $this->string(255)->null(),
            'sort'                  => $this->integer()->defaultValue(0)->null(),
            'star'                  => $this->integer()->defaultValue(0)->null(),
            'in_stock'              => $this->integer()->defaultValue(0)->null(),
            'in_weight'             => $this->integer()->defaultValue(0)->null(),
            'weight'                => $this->integer()->defaultValue(0)->null(),
            'metaName'              => $this->string(255)->null(),
            'metaDesc'              => $this->text()->null(),
            'metaKey'               => $this->text()->null(),
            'created_at'            => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->catalogTable}",
            "{{{$this->table}}}",'category_id',
            "{{{$this->catalogTable}}}",'id',
            'CASCADE', $onUpdateConstraint);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->catalogTable}", "{{{$this->catalogTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
