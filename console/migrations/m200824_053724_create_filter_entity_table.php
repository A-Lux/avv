<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filter_entity}}`.
 */
class m200824_053724_create_filter_entity_table extends Migration
{
    public $table               = 'filter_entity';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'name'              => $this->string(255)->null(),
            'type'              => $this->integer()->defaultValue(1)->null(),
            'position'          => $this->integer()->defaultValue(1)->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
