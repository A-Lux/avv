<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filter_attribute}}`.
 */
class m200824_053810_create_filter_attribute_table extends Migration
{
    public $table               = 'filter_attribute';
    public $entityTable         = 'filter_entity';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'entity_id'         => $this->integer()->notNull(),
            'name'              => $this->text()->null(),
            'sort'              => $this->integer()->null(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->entityTable}",
            "{{{$this->table}}}",'entity_id',
            "{{{$this->entityTable}}}",'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->entityTable}",
            "{{{$this->entityTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
