<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%filter_value}}`.
 */
class m200824_053826_create_filter_value_table extends Migration
{
    public $table               = 'filter_value';
    public $attributeTable      = 'filter_attribute';
    public $productTable        = 'products';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                => $this->primaryKey(),
            'attribute_id'      => $this->integer()->notNull(),
            'product_id'        => $this->integer()->notNull(),
            'created_at'        => $this->timestamp()->defaultValue(null),
        ], $tableOptions);


        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->attributeTable}",
            "{{{$this->table}}}", 'attribute_id',
            "{{{$this->attributeTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->attributeTable}",
            "{{{$this->attributeTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
