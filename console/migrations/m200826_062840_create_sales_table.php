<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sales}}`.
 */
class m200826_062840_create_sales_table extends Migration
{
    public $table               = 'sales';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'status'        => $this->integer()->defaultValue(1)->null(),
            'name'          => $this->string(255)->notNull(),
            'slug'          => $this->text()->null(),
            'title'         => $this->string(255)->null(),
            'content'       => $this->text()->null(),
            'image'         => $this->string(255)->null(),
            'sort'          => $this->integer()->null(),
            'metaName'      => $this->string(255)->null(),
            'metaDesc'      => $this->text()->null(),
            'metaKey'       => $this->text()->null(),
            'created_at'    => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable("{{{$this->table}}}");
    }
}
