<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%customers}}`.
 */
class m200901_111108_create_customers_table extends Migration
{
    public $table               = 'customers';
    public $userTable           = 'user';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'user_id'                   => $this->integer()->notNull(),
            'username'                  => $this->string(255)->null(),
            'phone'                     => $this->string(128)->null(),
            'birthday'                  => $this->date()->null(),
            'image'                     => $this->text()->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}",
            "{{{$this->table}}}", 'user_id',
            "{{{$this->userTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}",
            "{{{$this->userTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
