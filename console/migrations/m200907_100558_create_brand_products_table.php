<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%brand_products}}`.
 */
class m200907_100558_create_brand_products_table extends Migration
{
    public $table               = 'brand_products';
    public $brandTable          = 'brands';
    public $productTable        = 'products';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'brand_id'                  => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->brandTable}",
            "{{{$this->table}}}", 'brand_id',
            "{{{$this->brandTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->table}}}", 'product_id',
            "{{{$this->productTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->brandTable}",
            "{{{$this->brandTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
