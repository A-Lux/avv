<?php

use yii\db\Migration;

/**
 * Class m200907_101540_add_brands_table
 */
class m200907_101540_add_brands_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('brands', 'isMain', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('brands', 'isMain');
    }
}
