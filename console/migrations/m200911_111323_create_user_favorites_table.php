<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_favorites}}`.
 */
class m200911_111323_create_user_favorites_table extends Migration
{
    public $table               = 'user_favorites';
    public $userTable           = 'user';
    public $productTable        = 'products';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'user_id'                   => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->userTable}", "{{{$this->table}}}", 'user_id', "{{{$this->userTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
        $this->addForeignKey("fk_{$this->table}_{$this->productTable}", "{{{$this->table}}}", 'product_id', "{{{$this->productTable}}}", 'id', 'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->userTable}_{$this->table}", "{{{$this->userTable}}}");
        $this->dropForeignKey("fk_{$this->productTable}_{$this->table}", "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
