<?php

use yii\db\Migration;

/**
 * Class m200918_063541_add_columns_orders_table
 */
class m200918_063541_add_columns_orders_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('orders', 'typeDelivery', $this->integer()->null());
        $this->addColumn('orders', 'country', $this->string(255)->null());
        $this->addColumn('orders', 'city', $this->string(255)->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('orders', 'typeDelivery');
        $this->dropColumn('orders', 'country');
        $this->dropColumn('orders', 'city');
    }
}
