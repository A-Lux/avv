<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%delivery_address}}`.
 */
class m200918_064128_create_delivery_address_table extends Migration
{
    public $table               = 'delivery_address';
    public $cityTable           = 'cities';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'city_id'                   => $this->integer()->null(),
            'status'                    => $this->integer()->defaultValue(1)->null(),
            'address'                   => $this->string(255)->null(),
            'longitude'                 => $this->string(255)->null(),
            'latitude'                  => $this->string(255)->null(),
            'created_at'                => $this->timestamp()->defaultValue(null),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->cityTable}",
            "{{{$this->table}}}", 'city_id',
            "{{{$this->cityTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->cityTable}",
            "{{{$this->cityTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
