<?php

use yii\db\Migration;

/**
 * Class m201222_104840_add_columns_products_table
 */
class m201222_104840_add_columns_products_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('products', 'size', $this->integer()->null());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('products', 'size');
    }
}
