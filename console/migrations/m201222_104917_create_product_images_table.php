<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_images}}`.
 */
class m201222_104917_create_product_images_table extends Migration
{
    public $table               = 'product_images';
    public $productTable        = 'products';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'            => $this->primaryKey(),
            'product_id'    => $this->integer()->null(),
            'name'          => $this->string(255)->null(),
            'image'         => $this->text()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }

        $this->addForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->table}}}", 'product_id',
            "{{{$this->productTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
