<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%type_products}}`.
 */
class m201222_105111_create_type_products_table extends Migration
{
    public $table               = 'type_products';
    public $typeTable           = 'product_types';
    public $productTable        = 'products';

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable("{{{$this->table}}}", [
            'id'                        => $this->primaryKey(),
            'type_id'                   => $this->integer()->null(),
            'product_id'                => $this->integer()->null(),
            'created_at'                => $this->dateTime()->null(),
        ], $tableOptions);

        $onUpdateConstraint = 'RESTRICT';
        if ($this->db->driverName === 'sqlsrv') {
            $onUpdateConstraint = 'NO ACTION';
        }
        $this->addForeignKey("fk_{$this->table}_{$this->typeTable}",
            "{{{$this->table}}}", 'type_id',
            "{{{$this->typeTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);

        $this->addForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->table}}}", 'product_id',
            "{{{$this->productTable}}}", 'id',
            'CASCADE', $onUpdateConstraint);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey("fk_{$this->table}_{$this->typeTable}",
            "{{{$this->typeTable}}}");
        $this->dropForeignKey("fk_{$this->table}_{$this->productTable}",
            "{{{$this->productTable}}}");
        $this->dropTable("{{{$this->table}}}");
    }
}
