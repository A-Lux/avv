<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-frontend',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'frontend\controllers',
    'components' => [
        'request' => [
            'baseUrl'   => '',
            'csrfParam' => '_csrf-frontend',
        ],
        'user' => [
            'loginUrl' => '/login',
        ],
        'session' => [
            // this is the name of the session cookie used for login on the frontend
            'name' => 'advanced-frontend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'formatter' => [
            'timeZone' => 'Asia/Almaty',
            'dateFormat' => 'dd MMMM', //Date format to used here
            'datetimeFormat' => 'php:d-m-Y H:i:s',
            'timeFormat' => 'php:h:i:s A',
            'decimalSeparator' => '.',
            'thousandSeparator' => '',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [

                'profile/<action:\w+>'              => 'profile/index',
                'basket/<action:\w+>'               => 'basket/index',
            ],
        ],

    ],
    'params' => $params,
];
