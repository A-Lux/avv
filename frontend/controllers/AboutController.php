<?php

namespace frontend\controllers;

use common\models\About;
use Yii;

/**
 * About controller
 */
class AboutController extends FrontendController
{
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $model  = About::getOne();

        return $this->render('index', [
            'model' => $model,
        ]);
    }
}
