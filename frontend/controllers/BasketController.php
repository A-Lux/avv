<?php

namespace frontend\controllers;

use Yii;

/**
 * Basket controller
 */
class BasketController extends FrontendController
{
    /**
     * Displays basket page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }
}
