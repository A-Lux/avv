<?php

namespace frontend\controllers;

use common\models\Brands;
use Yii;

/**
 * Brands controller
 */
class BrandsController extends FrontendController
{
    /**
     * Displays brands page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $brands     = Brands::getAll();

        return $this->render('index', [
            'brands'        => $brands,
        ]);
    }
}
