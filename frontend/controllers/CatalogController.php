<?php

namespace frontend\controllers;

use common\models\Catalog;
use Yii;
use yii\db\ActiveRecord;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Catalog controller
 */
class CatalogController extends FrontendController
{
    /**
     * Displays catalog page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * Displays a single model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model  = $this->findModel($id);

        return $this->render('view', [
            'model'     => $model
        ]);
    }

    /**
     * Finds the Catalog model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $activeRecord = new Catalog();

        if (null === $model = $activeRecord::findOne($id)) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }

    public  function actionTest()
    {
        $model      = Catalog::find()->all();

        $result     = Catalog::getCatalogItems($model);

        return $result;
    }
}

