<?php

namespace frontend\controllers;

use common\models\Contact;
use Yii;

/**
 * Contact controller
 */
class ContactsController extends FrontendController
{
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $contact       = Contact::getOne();

        return $this->render('index', [
            'contact'  => $contact,
        ]);
    }
}
