<?php

namespace frontend\controllers;

use common\models\Contact;
use Yii;

/**
 * Order controller
 */
class OrderController extends FrontendController
{
    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }
}
