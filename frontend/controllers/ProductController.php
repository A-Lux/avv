<?php

namespace frontend\controllers;

use common\models\ProductImages;
use common\models\Products;
use Yii;
use yii\db\ActiveRecord;
use yii\web\NotFoundHttpException;

/**
 * Product controller
 */
class ProductController extends FrontendController
{
    /**
     * Displays products page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }

    /**
     * Displays a single model.
     * @param $id
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        $model  = $this->findModel($id);
        $images = ProductImages::findAll(['product_id' => $id]);

        return $this->render('view', [
            'model'     => $model,
            'images'    => $images,
        ]);
    }

    /**
     * Finds the Products model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param string $id
     * @return ActiveRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $activeRecord = new Products();

        if (null === $model = $activeRecord::findOne($id)) {
            throw new NotFoundHttpException('Указанная страница не найдена.');
        }

        return $model;
    }
}

