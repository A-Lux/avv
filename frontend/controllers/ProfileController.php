<?php

namespace frontend\controllers;

use Yii;

/**
 * Profile controller
 */
class ProfileController extends FrontendController
{
    /**
     * Displays profile page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
        ]);
    }
}
