<?php

namespace frontend\controllers;

use common\models\Brands;
use common\models\Products;
use common\models\UserFavorites;
use Yii;

/**
 * Search controller
 */
class SearchController extends FrontendController
{
    /**
     * Displays search page.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $text   = '';
        if(!empty($_GET['text'])) {
            $text = $_GET['text'];
            $products   = Products::find()->andFilterWhere(['like', 'name', $text])->all();

            return $this->render('index', [
                'text'      => $text,
                'products'  => $products,
            ]);
        }else {
            return $this->render('index', [
                'text'      => $text,
            ]);
        }
    }

    public function actionView()
    {
        $text = $_GET['text'];

        return $this->render('view', [

        ]);
    }
}
