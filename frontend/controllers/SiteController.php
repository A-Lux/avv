<?php
namespace frontend\controllers;

use common\models\AboutMain;
use common\models\Banners;
use common\models\Brands;
use common\models\Catalog;
use common\models\ProductTypes;
use common\models\Sales;
use Yii;

/**
 * Site controller
 */
class SiteController extends FrontendController
{
    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $banners        = Banners::getBanners();
        $catalog        = Catalog::getCategoryParent();
        $sales          = Sales::getAll();
        $about          = AboutMain::getOne();
        $brands         = Brands::isMain();
        $types          = ProductTypes::getAll();

        return $this->render('index', [
            'banners'       => $banners,
            'catalog'       => $catalog,
            'sales'         => $sales,
            'about'         => $about,
            'brands'        => $brands,
            'types'         => $types,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
