<?php

/* @var $this View */

/* @var $model      About */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\About;

?>

<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a><span> / О компании</span>
                    <h2>О компании</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                <?= $this->render('/partials/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="about_descr" data-aos="fade-up" data-aos-anchor-placement="top-bottom">
                    <h2><?= $model->title; ?></h2>

                    <?= $model->content; ?>
                </div>
            </div>
        </div>
    </div>
</div>
