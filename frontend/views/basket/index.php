<?php


?>

<div class="about" data-aos="fade-down" data-aos-easing="linear" data-aos-duration="1500">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a><span> / Корзина</span>
                    <h2>Корзина</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <!-- <div class="col-md-3">
                <?= $this->render('/partials/sidebar') ?>
            </div> -->
            <div class="col-md-12">
                <div id="app">
                    <Cart></Cart>
                </div>
            </div>
        </div>
    </div>
</div>
