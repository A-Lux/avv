<?php

/* @var $this View */

/* @var $brands      Brands */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Brands;

?>

<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a><span> / Бренды</span>
                    <h2>Бренды</h2>
                </div>
            </div>
        </div>
        <div class="row brand_wrap">
            <? foreach ($brands as $brand): ?>
                <div class="col-md-3" data-aos="flip-left" data-aos-once="true">
                    <div class="brand_items">
                        <img src="<?= $brand->getImage(); ?>" alt="">
                        <?= $brand->content; ?>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
    </div>
</div>
