<?php


?>

<div class="about" data-aos="fade-up" data-aos-duration="1500">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a><span> / Каталог товаров</span>
                    <h2>Все категории</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('/partials/sidebar') ?>
            </div>
            <div class="col-md-9">
                <div id="app">
                    <Maincatalog></Maincatalog>
                </div>
            </div>
        </div>
    </div>
</div>
