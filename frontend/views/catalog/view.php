<?php

use yii\helpers\Html;
use yii\helpers\Url;
use common\models\Catalog;

/* @var $this               yii\web\View */
/* @var $model              Catalog */

?>

<div class="about" data-aos="fade-up" data-aos-duration="1500">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a>
                    <span>/
                        <a href="/catalog">Каталог товаров</a> /
                        <?= $model->name ?>
                    </span>
                    <h2><?= $model->name ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('/partials/sidebar') ?>
            </div>
            <div class="col-md-9">
                <div id="app">
                    <Catalog></Catalog>
                </div>
            </div>
        </div>
    </div>
</div>
