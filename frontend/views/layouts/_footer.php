<?php

use yii\helpers\Url;
use common\models\Menu;

$menu       = \Yii::$app->view->params['menuFooter'];
$contacts   = \Yii::$app->view->params['contacts'];
$logo       = \Yii::$app->view->params['logoFooter'];

?>

<div class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="footer-links">
                <nav>
                    <? foreach ($menu as $value): ?>
                        <li>
                            <a class="hvr-bounce-in" href="<?= $value->url; ?>">
                                <?= $value->name; ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                </nav>
            </div>
        </div>
    </div>
    <div class="footer-medium" data-aos="fade-up" data-aos-once="true" data-aos-anchor-placement="top-center">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="footer-info-contact">
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/mail-footer.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Напишите нам </p>
                                <a href="mailto:<?= $contacts->email; ?>">
                                    <?= $contacts->email; ?>
                                </a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/phone.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Звоните нам </p>
                                <a href="">
                                    <?= $contacts->phone; ?>
                                </a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/fax.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Факс </p>
                                <a href="">
                                    <?= $contacts->fax; ?>
                                </a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/map.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Адрес </p>
                                <span>
                                    <?= $contacts->address; ?>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="footer-info">
                        <div id="map">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4 col-12">
                    <div class="footer-bottom-info">
                        <img src="<?= $logo->getImage(); ?>" alt="">
                        <p class='development-logo'>Разработано в <a href="https://www.a-lux.kz/"><img src="../images/logo-a-lux.png" alt=""></a></p>
<!--                        <a href="https://www.a-lux.kz/" class="alux_logo" target="_blank">-->
<!--                            <span>Сделано в </span>-->
<!--                            <img src="/images/A-lux.svg" alt="">-->
<!--                        </a>-->
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-12">
                    <div class="footer-bottom-info" >
                        <p>© 2017 - 2020 AVV industrial. Все права защищенты</p>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4 col-12">
                    <div class="footer-bottom-info social-icons">
                        <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                        <a href=""><i class="fa fa-vk" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>