<?php

use yii\helpers\Url;
use common\models\Menu;


$menu       = \Yii::$app->view->params['menuHeader'];
$contacts   = \Yii::$app->view->params['contacts'];
$logo       = \Yii::$app->view->params['logoHeader'];

?>

<div class="header" data-aos="fade-right" data-aos-once="true">
    <div class="container-fluid">
        <div class="header-inner">
            <div class="row">
                <div class="col-xl-6">
                    <div class="header-links">
                        <nav>
                            <li class="logo hvr-grow">
                                <a href="/">
                                    <img src="<?= $logo->getImage(); ?>" alt="">
                                </a>
                            </li>
                            <? foreach ($menu as $value): ?>
                                <li>
                                    <a class="hvr-bounce-in" href="<?= $value->url; ?>">
                                        <?= $value->name; ?>
                                    </a>
                                </li>
                            <? endforeach; ?>
                        </nav>
                    </div>
                </div>
                <div class="col-xl-4 pr-0">
                    <div class="header-info-inner">
                        <div class="header-info-box">
                            <div class="header-text">
                                <p>Наш основной сайт</p>
                                <a href="http://www.avv.kz/" target="_blank">
                                    avv.kz
                                </a>
                            </div>
                        </div>
                        <div class="header-info-box">
                            <div class="header-icon">
                                <img src="/images/mail.svg" alt="">
                            </div>
                            <div class="header-text">
                                <p>Напишите нам</p>
                                <a href="mailto:<?= $contacts->email; ?>">
                                    <?= $contacts->email; ?>
                                </a>
                            </div>
                        </div>
                        <div class="header-info-box">
                            <div class="header-icon">
                                <img src="/images/phone.svg" alt="">
                            </div>
                            <div class="header-text">
                                <p>Звоните нам </p>
                                <a href="tel:+77273902330"><?= $contacts->phone; ?></a> 
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-2 pr-0 col-12">
                    <div class="header-icons-inner">
                        <a href="#" class="header-icons hvr-grow profile">
                            <img class="noactive" src="/images/Profile.svg" alt="">
                            <img class="active_img" src="/images/profile_red.svg" alt="">
                        </a>
                        <a href="/search" class="header-icons hvr-grow search">
                            <img src="/images/search.svg" alt="">
                        </a>
                        <a href="#" class="header-icons hvr-grow favorite_page">
                            <img src="/images/favorite.svg" alt="">
                        </a>
                        <a href="/basket" class="header-icons hvr-grow">
                            <img src="/images/basket shop.svg" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="burger-menu-content">
        <a href="/">
            <img src="<?= $logo->getImage(); ?>" alt="">
        </a>
        <a href="javascript:void(0);" class="header-icons header-icons-mob profile">
            <img class="noactive" src="/images/Profile.svg" alt="">
            <img class="active_img" src="/images/profile_red.svg" alt="">
        </a>
        <a href="#" class="header-icons header-icons-mob search">
            <img src="/images/search.svg" alt="">
        </a>
        <a href="/basket" class="header-icons header-icons-mob">
            <img src="/images/basket shop.svg" alt="">
        </a>
        <a href="#" class="burger-menu-btn">
            <span class="burger-menu-lines"></span>
        </a>
    </div>
    <div class="nav-panel-mobil">
        <div class="container">
            <nav class="navbar-expand-lg navbar-light">
                <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                    <li class="nav-item">
                        <a href=""><img src="/images/home-image.png" class="home-image" alt="">Магазин</a>
                    </li>
                    <? foreach ($menu as $value): ?>
                        <li class="nav-item">
                            <a href="<?= $value->url; ?>">
                                <?= $value->name; ?>
                            </a>
                        </li>
                    <? endforeach; ?>
                    <div class="header-info-box">
                        <div class="header-icon">
                            <img src="/images/mail.svg" alt="">
                        </div>
                        <div class="header-text">
                            <p>Напишите нам</p>
                            <a><?= $contacts->email; ?></a>
                        </div>
                    </div>
                    <div class="header-info-box">
                        <div class="header-icon">
                            <img src="/images/phone.svg" alt="">
                        </div>
                        <div class="header-text">
                            <p>Звоните нам </p>
                            <a><?= $contacts->phone; ?></a>
                        </div>
                    </div>
                </ul>
            </nav>
        </div>
    </div>
    <div class="login_block">
        <h3>Личный кабинет</h3>
        <div class="login_btns">
            <a class="login" href="#">Войти</a>
            <a class="register" href="#">Зарегистрироваться</a>
        </div>
    </div>

    <!-- Login modal -->
    <div class="modal" id="login_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 358px; margin: 0 auto; margin-top: 75px; margin-bottom: 40px;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Войти</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="login_auto">
                        <h3>E-mail</h3>
                        <input class="login_email" type="email">
                        <h3>Пароль</h3>
                        <input class="login_password" type="password">
                        <div class="modal-footer">
                            <button class="modal_btn login_btn" href="#">Войти</button>
                            <a class="modal_a register_m" href="#">Регистрация</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>

    <!-- Register modal -->
    <div class="modal" id="register_modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" style="width: 358px; margin: 0 auto; margin-top: 25px;" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">регистрация</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form class="reg">
                        <h3>Имя</h3>
                        <input class="name" type="text" required>
                        <h3>Телефон</h3>
                        <input class="phone" type="phone" required>
                        <h3>E-mail</h3>
                        <input class="email" type="email" required>
                        <h3>Пароль</h3>
                        <input class="password" type="password" required>
                        <h3>Повторите пароль</h3>
                        <input class="repassword" type="password" required>
                        <div class="modal-footer">
                            <button class="modal_btn" type="submit">Зарегистрироваться</button>
                            <a class="modal_a login_m" href="#">Войти в аккаунт</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>