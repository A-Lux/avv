<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\ToastrAlert;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">

    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="/css/main.css">
        <link rel="stylesheet" href="/css/hover-min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.compat.css" />
        <?php $this->head() ?>
    </head>

    <body>
    <?php $this->beginBody() ?>
    <div id="app">
        <!-- HEADER -->
        <?= $this->render('_header') ?>
        <!-- END HEADER -->

        <?= \yii2mod\alert\Alert::widget() ?>
        <!-- CONTENT -->
        <?= $content ?>
        <!-- END CONTENT -->

        <!-- FOOTER -->
        <?= $this->render('_footer') ?>
        <!-- END FOOTER -->
    </div>
    <script src="https://api-maps.yandex.ru/2.1/?apikey=8350983e-1e36-4ac2-95ed-62859584bf6f&lang=ru_RU"></script>
    <script src="/js/main.js"></script>
    <?php $this->endBody() ?>
    </body>

    </html>
<?php $this->endPage() ?>