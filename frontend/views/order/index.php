<?php

?>

<div class="about" data-aos="fade-up" data-aos-easing="linear" data-aos-duration="1500">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / Оформление заказа</span>
          <h2>Оформление заказа</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="app">
          <Checkout></Checkout>
        </div>
      </div>
    </div>
  </div>  
</div>
