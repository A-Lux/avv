<?php

use yii\helpers\Url;

$catalog    = Yii::$app->view->params['catalog'];

?>

<div class="sidebar">
    <ul>
        <? foreach ($catalog as $category): ?>
            <li>
                <a href="<?= Url::to(['catalog/view', 'id' => $category->id]) ?>">
                    <span><?= $category->name; ?></span>
                    <img src="/images/arrow.png" alt="">
                </a>
            </li>
        <? endforeach; ?>
    </ul>
</div>
