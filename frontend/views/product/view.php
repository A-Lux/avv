<?php

/* @var $this View */

/* @var $model      Products */
/* @var $images     ProductImages */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use common\models\Products;
use yii\helpers\ArrayHelper;
use common\models\ProductImages;

?>

<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="navigation_links">
                    <a href="/">Главная</a><span> / </span>
                    <a href="/catalog">Каталог товаров</a>
                    <span> /</span>
                    <a href="<?= Url::to(['catalog/view', 'id' => $model->category_id]) ?>">
                        <?= $model->category->name; ?>
                    </a>
                    <span> / </span>
                    <a href="#">
                    <?= $model->name; ?>
                    </a>
                    <h2><?= $model->name; ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-3">
                <?= $this->render('/partials/sidebar'); ?>
            </div>
            <div class="col-md-9">
                <div class="row">
                    <div class="col-md-4">
                            <div class="images">
                                <img src="<?= $model->getImage(); ?>" alt="">
                                <? foreach ($images as $image): ?>
                                    <img src="<?= $image->getImage(); ?>" alt="">
                                <? endforeach; ?>
                            </div>
                        <div class="imagesnew_dotted" style="transform: none !important;">
                            <img class="active" src="<?= $model->getImage(); ?>">
                            <? foreach ($images as $image): ?>
                                <img src="<?= $image->getImage(); ?>" alt="">
                            <? endforeach; ?>
                        </div>
                    </div>
                    <div class="col-md-4 card_title_wrap">
                        <div class="card_title">
                            <div class="card_title_text">
                                <h2>Артикул: <?= $model->code; ?></h2>
                            </div>
                            <div class="star">
                                <img src="/images/gold_star.png" alt="gold">
                                <img src="/images/gold_star.png" alt="gold">
                                <img src="/images/gold_star.png" alt="gold">
                                <img src="/images/gold_star.png" alt="gold">
                                <img src="/images/gold_star.png" alt="gold">
                                <img src="/images/normal_star.png" alt="normal">
                                <span>(1)</span>
                            </div>
                        </div>
                        <div class="card_title_descr">
                            <?= $model->content; ?>
                        </div>
                        <div class="card_title_icons">
                            <div class="card_title_icon_descr">
                                <img src="/images/icon.png" alt="">
                                <p>Бесплатная доставка по Москве <strong> от 15 000 руб.</strong></p>
                            </div>
                            <div class="card_title_icon_descr">
                                <img src="/images/2.png" alt="">
                                <p>Онлайн-консультации по любым вопросам</p>
                            </div>
                        </div>
                        <div class="card_title_footer">
                            <h3>Основные характеристики</h3>
                            <div class="car_title_footer_descr">
                                <div class="car_title_footer_row">
                                    <div class="footer_h">Производитель</div>
                                    <div class="footer_p"><?= $model->manufacturer ?></div>
                                </div>
                                <div class="car_title_footer_row">
                                    <div class="footer_h">Производство</div>
                                    <div class="footer_p"><?= $model->production ?></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="card_payment">
                            <div class="card_head">
                                <h3 class="price-information price-pay"><?= $model->price ?></h3> тг. за 1 шт</span>
                            </div>
                            <div class="card_payment_descr">
                                <div class="card_payment_1">
                                    <img src="/images/payment_check.png" alt="">
                                    <span>
                                        <?= ArrayHelper::getValue(Products::stockDescription(),
                                            $model->in_stock);
                                        ?>
                                    </span>
                                </div>
                                <div class="card_payment_2">
                                    <button class="minus">-</button>
                                    <input class="payment_count" type="text" value="1" readonly="readonly">
                                    <button class="plus">+</button>
                                </div>
                            </div>
                            <div class="brands-favorite">
                                <a class="add_basket" href="javascript:void(0);" data-id="<?= $model->id; ?>">Добавить в корзину</a>
                                <a class="favorite-icon" href="javascript:void(0);"  data-id="<?= $model->id; ?>"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
                            </div>
                            <div class="payment_variant">
                                <h2>Способы оплаты</h2>
                                <div class="payment_variant_img">
                                    <img src="/images/visa.png" alt="">
                                    <img src="/images/mir.png" alt="">
                                    <img src="/images/mastercard.png" alt="">
                                </div>
                            </div>
                            <div class="card_footer">
                                <a href="#"><img src="/images/pay_icon.png" alt=""><span>Все способы оплаты</span></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card_tabs">
                            <nav>
                                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                                        Описание
                                    </a>
                                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                                        Характеристики
                                    </a>
                                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                                        Вопросы и ответы (0)
                                    </a>
                                </div>
                            </nav>
                            <div class="tab-content" id="nav-tabContent">
                                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                                    <div class="card_tabs_descr">
                                        <?= $model->content; ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                                    <div class="card_tabs_descr">
                                        <?= $model->characteristic; ?>
                                    </div>
                                </div>
                                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                                    <div class="card_tabs_descr">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
