<?php
use yii\helpers\Url;
use common\models\Products;

/* @var $text */
/* @var $products Products */
?>
<div class="search_page">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-12 search_wrap">
                <form action="/search" method="get">
                    <input placeholder="Поиск каталог товаров..." type="text" name="text">
                </form>
                <img src="/images/search_icon.svg" alt="">
            </div>
        </div>
        <? if(!empty($text)): ?>
            <div class="search-title">
                <h1>Результаты по запросу:</h1>
                <h2><?= $text; ?></h2>
            </div>
        <? endif; ?>
        <div class="row home">
            <? if(!empty($products)): ?>
                <? foreach ($products as $product): ?>
                    <div class="col-xl-3 col-md-6">
                        <div class="brands-inner hvr-shrink">
                            <a href="<?= Url::to(['/product/view', 'id' => $product->id]); ?>">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="<?= $product->getImage(); ?>" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1><?= $product->name; ?></h1>
                                    <p>Артикуль: <?= $product->code; ?></p>
                                    <div class="brands-stars">
                                        <img src="/images/star.png" alt="">
                                        <img src="/images/star.png" alt="">
                                        <img src="/images/star.png" alt="">
                                        <img src="/images/star.png" alt="">
                                        <img src="/images/star.png" alt="">
                                    </div>
                                    <div class="brand_descr">
                                        <?= $product->description; ?>
                                    </div>
                                    <h1 class="brands-price">
                                        <?= $product->price ?> тг/шт
                                    </h1>
                                </div>
                            </a>
                            <div class="brands-favorite">
                                <a class="add_basket" data-id="<?= $product->id; ?>"
                                   href="javascript:void(0);">
                                    Добавить в корзину
                                </a>
                                <a href="javascript:void(0);" data-id="<?= $product->id; ?>"
                                   class="favorite-icon">
                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                <? endforeach; ?>
            <? endif; ?>
        </div>
    </div>
</div>

