<?php

use yii\web\View;
use yii\helpers\Url;
use common\models\Banners;
use common\models\Catalog;
use common\models\Sales;
use common\models\AboutMain;
use common\models\Brands;
use common\models\ProductTypes;

/* @var $this View */
/* @var $banners Banners */
/* @var $catalog Catalog  */
/* @var $sales Sales  */
/* @var $about AboutMain  */
/* @var $brands Brands  */
/* @var $types ProductTypes  */

?>

<div class="home">
    <div class="container" data-aos="fade-up">
        <div class="slider-top">
            <? foreach ($banners as $banner): ?>
                <div class="home-slide">
                    <div class="home-slide-text">
                        <h1><?= $banner->title; ?> </h1>
                        <p><?= $banner->content ?></p> 
                        <? if($banner->isButton == 1): ?>
                            <a href="<?= $banner->linkButton; ?>">Подробнее</a>
                        <? endif; ?>
                    </div>
                </div>
            <? endforeach; ?>
        </div>
        <div class="title">
            <h1>Каталог товаров</h1>
            <a class="hvr-pulse-grow" href="/catalog">смотреть все</a>
        </div>
        <div class="catalog-inner">
            <? foreach ($catalog as $category): ?>
                <a href="">
                    <div class="catalog-link hvr-grow">
                        <a href="<?= Url::to(['/catalog/view', 'id' => $category->id]) ?>"
                           class="catalog-title">
                            <?= $category->name; ?>
                        </a>
                    </div>
                </a>
            <? endforeach; ?>
        </div>
        <div class="title">
            <h1>Акции и скидки</h1>
        </div>
        <div class="discounts-slider">
            <? foreach ($sales as $sale): ?>
                <a href="#">
                    <div class="discount-slide hvr-shrink">
                        <div class="discount-image">
                            <img src="<?= $sale['image'] ?>" alt="">
                        </div>
                        <div class="discount-content">
                            <h1><?= $sale['title'] ?> </h1>
                            <?= $sale['content'] ?>
                        </div>
                    </div>
                </a>
            <? endforeach; ?>
        </div>
    </div>
    <div class="products" data-aos="zoom-in" data-aos-once="true">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="title">
                        <h1>Товары от бренда</h1>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="list-group" id="list-tab" role="tablist">
                        <? $count   = 0; ?>
                        <? foreach ($brands as $brand): ?>
                            <? $count++; ?>
                            <a class="list-group-item list-group-item-action
                                <?= $count == 1 ? 'active' : ''; ?>"
                               id="list-brands-<?= $brand->id; ?>-list"
                               data-toggle="list" href="#list-brands-<?= $brand->id; ?>"
                               role="tab"
                               aria-controls="brands-<?= $brand->id; ?>">
                                <?= $brand->name; ?>
                            </a>
                        <? endforeach; ?>
                    </div>
                </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <? $count   = 0; ?>
                <? foreach ($brands as $brand): ?>
                    <? $count++; ?>
                    <div class="tab-pane fade <?= $count == 1 ? 'show active' : ''; ?>"
                         id="list-brands-<?= $brand->id; ?>" role="tabpanel"
                         aria-labelledby="list-brands-<?= $brand->id; ?>-list">
                        <div class="row">
                            <? foreach ($brand->brandProducts as $brandProduct): ?>
                            <? $product = $brandProduct->product; ?>
                                <div class="col-xl-3 col-md-6">
                                    <div class="brands-inner">
                                        <a href="<?= Url::to(['/product/view', 'id' => $product->id]); ?>">
                                            <div class="brands-hashtags">
                                                <div class="new-brands">
                                                    <p>New</p>
                                                </div>
                                                <div class="hit-brands">
                                                    <p>Хит</p>
                                                </div>
                                                <div class="stock-brands">
                                                    <p>В наличии</p>
                                                </div>
                                            </div>
                                            <div class="brands-image">
                                                <img src="<?= $product->getImage(); ?>" alt="">
                                            </div>
                                            <div class="brands-info">
                                                <h1><?= $product->name; ?></h1>
                                                <p>Артикуль: <?= $product->code; ?></p>
                                                <div class="brands-stars">
                                                    <img src="images/star.png" alt="">
                                                    <img src="images/star.png" alt="">
                                                    <img src="images/star.png" alt="">
                                                    <img src="images/star.png" alt="">
                                                    <img src="images/star.png" alt="">
                                                </div>
                                                <div class="brand_descr">
                                                    <?= $product->description; ?>
                                                </div>
                                                <h1 class="brands-price"><?= $product->price ?> тг/шт</h1>
                                            </div>
                                        </a>
                                        <div class="brands-favorite" >
                                            <a class="add_basket" data-id="<?= $product->id; ?>" href="javascript:void(0);">Добавить в корзину</a>
                                            <a href="javascript:void(0);" data-id="<?= $product->id; ?>" class="favorite-icon">
                                                <i class="fa fa-heart-o"
                                                    aria-hidden="true"></i>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            <? endforeach; ?>
                        </div>
                    </div>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="popular-products" data-aos="fade-up" data-aos-once="true">
        <div class="container">
            <div class="list-group" id="list-tab" role="tablist">
                <? $countType   = 0; ?>
                <? foreach($types as $type): ?> <? $countType++; ?>
                    <? if($type->typeProducts): ?>
                        <a class="list-group-item list-group-item-action
                            <?= $countType == 1 ? 'active' : ' ' ?> "
                           id="list-products-<?= $type->id; ?>-list" data-toggle="list"
                           href="#list-products-<?= $type->id; ?>" role="tab"
                           aria-controls="products-<?= $type->id; ?>">
                            <?= $type->name; ?>
                        </a>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <? $countType   = 0; ?>
                <? foreach($types as $type): ?> <? $countType++; ?>
                    <? if($type->typeProducts): ?>
                        <div class="tab-pane fade <?= $countType == 1 ? 'show active' : ' ' ?> "
                             id="list-products-<?= $type->id; ?>" role="tabpanel"
                             aria-labelledby="list-products-<?= $type->id; ?>-list">
                            <div class="row">
                                <? foreach ($type->typeProducts as $typeProduct): ?>
                                <? $product = $typeProduct->product; ?>
                                    <div class="col-xl-3 col-md-6">
                                        <div class="brands-inner">
                                            <a href="<?= Url::to(['/product/view', 'id' => $product->id]); ?>">
                                                <div class="brands-hashtags">
                                                    <div class="new-brands">
                                                        <p>New</p>
                                                    </div>
                                                    <div class="hit-brands">
                                                        <p>Хит</p>
                                                    </div>
                                                    <div class="stock-brands">
                                                        <p>В наличии</p>
                                                    </div>
                                                </div>
                                                <div class="brands-image">
                                                    <img src="<?= $product->getImage(); ?>" alt="">
                                                </div>
                                                <div class="brands-info">
                                                    <h1><?= $product->name; ?></h1>
                                                    <p>Артикуль: <?= $product->code; ?></p>
                                                    <div class="brands-stars">
                                                        <img src="/images/star.png" alt="">
                                                        <img src="/images/star.png" alt="">
                                                        <img src="/images/star.png" alt="">
                                                        <img src="/images/star.png" alt="">
                                                        <img src="/images/star.png" alt="">
                                                    </div>
                                                    <div class="brand_descr">
                                                        <?= $product->description; ?>
                                                    </div>
                                                    <h1 class="brands-price">
                                                        <?= $product->price ?> тг/шт
                                                    </h1>
                                                </div>
                                            </a>
                                            <div class="brands-favorite">
                                                <a class="add_basket" data-id="<?= $product->id; ?>"
                                                   href="javascript:void(0);">
                                                    Добавить в корзину
                                                </a>
                                                <a href="javascript:void(0);" data-id="<?= $product->id; ?>"
                                                   class="favorite-icon">
                                                    <i class="fa fa-heart-o" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                <? endforeach; ?>
                            </div>
                        </div>
                    <? endif; ?>
                <? endforeach; ?>
            </div>
        </div>
    </div>
    <div class="shop-avv" data-aos-once="true" data-aos="fade-down">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12">
                    <div class="title">
                        <h1><?= $about->title; ?></h1>
                    </div>
                    <div class="shop-text">
                        <?= $about->content; ?>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="shop-image">
                        <img src="<?= $about->getImage(); ?>" alt="">
                        <?= $about->text; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
