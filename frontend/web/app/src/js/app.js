import Vue from 'vue'
import router from './router/router'
import store from './vuex/store'

import Cabinet from './views/Cabinet.vue'
import Cart from './views/Cart.vue'
import Catalog from './views/Catalog.vue'
import Maincatalog from './views/Maincatalog.vue'
import Checkout from './views/Checkout.vue'
import { Datetime } from 'vue-datetime'
import 'vue-datetime/dist/vue-datetime.css'
import VueSweetalert2 from 'vue-sweetalert2'
import 'sweetalert2/dist/sweetalert2.min.css';

Vue.use(Datetime)
Vue.use(VueSweetalert2);
Vue.component("Cabinet", Cabinet)
Vue.component("Catalog", Catalog)
Vue.component("Maincatalog", Maincatalog)
Vue.component("Cart", Cart)
Vue.component("Checkout", Checkout)

new Vue({
  el: "#app",
  router,
  store,
})