const { default: Axios } = require("axios");
const { default: Swal } = require("sweetalert2");

window.$ = window.jQuery = require("jquery");
require('popper.js');
require('bootstrap');
require('slick-slider');
require('jquery-mask-plugin');
require('sweetalert2');
require('animate.css');
import AOS from 'aos';
import 'aos/dist/aos.css';

AOS.init();

let name = document.querySelector('.name')
let phone = document.querySelector('.phone')
let email = document.querySelector('.email')
let password = document.querySelector('.password')
let repassword = document.querySelector('.repassword')
let reg = document.querySelector('.reg')
let login_auto = document.querySelector('.login_auto')
let login_email = document.querySelector('.login_email')
let login_password = document.querySelector('.login_password')
let exit = document.querySelector('.exit')
let add_basket = document.querySelectorAll('.add_basket')
let favorite = document.querySelectorAll('.favorite-icon')
let favorite_page = document.querySelector('.favorite_page')
let data = {}
let basketArr;

if (favorite_page) {
  favorite_page.addEventListener('click', () => {
    if (localStorage.getItem('token')) {
        window.location.replace(window.location.origin + '/profile/favorite')
    } else {
      Swal.fire({
        icon: "error",
        title: "Пожалуйста зарегистрируйтесь!",
      })
    }
  })
}

  $('.brands-price').text((i, text) => {
    const [ price, currency ] = text.split(' ');
    return `${(+price).toLocaleString()} ${currency}`;
  });

  $('.card_head h2').text((i, text) => {
    const [ price, currency ] = text.split(' ');
    return `${(+price).toLocaleString()} ${currency}`;
  });

if (add_basket) {
  add_basket.forEach(item => {
    item.addEventListener('click', () => {
      let id = +item.getAttribute('data-id')
      let arrBasket = JSON.parse(localStorage.getItem('basketItem'))
      if (arrBasket.length == 0 || arrBasket.length == null) {
        arrBasket.push(id)
        Swal.fire({
          icon: "success",
          title: "Товар добавлен в корзину!",
          toast: true,
          position: "top-end",
          showConfirmButton: false,
          timer: 3000,
          timerProgressBar: true,
        })
        localStorage.setItem('basketItem', JSON.stringify(arrBasket))
      } else {
        if (arrBasket.find( prod_id => {return prod_id == id})) {
          Swal.fire({
            icon: "success",
            title: "Данный товар Вы уже добавили в корзину!",
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
          })
        } else {
          arrBasket.push(id)
          Swal.fire({
            icon: "success",
            title: "Товар добавлен в корзину!",
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
          })
          localStorage.setItem('basketItem', JSON.stringify(arrBasket))
        }
      }
    })
  })
}

if (favorite) {
  favorite.forEach(item => {
    item.addEventListener('click', () => {
      let id = +item.getAttribute('data-id')
      let token = localStorage.getItem('token')
      const config = {
        headers: { Authorization: `Bearer ${token}` }
      };
      Axios
        .post('/api/favorites/create', { product: id }, config)
        .then(res => {
          Swal.fire({
            icon: "success",
            title: res.data.message,
            toast: true,
            position: "top-end",
            showConfirmButton: false,
            timer: 3000,
            timerProgressBar: true,
          })
        })
    })
  })
}

if(localStorage.getItem('basketItem')) {
  basketArr = localStorage.getItem('basketItem')
}else{
  localStorage.setItem('basketItem', JSON.stringify([]))
}

if (exit) {
  exit.addEventListener('click', () => {
    localStorage.removeItem('token')
    window.location.replace('/')
  })
}

login_auto.addEventListener('submit', e => {
  e.preventDefault()
  console.log(login_email.value);
  console.log(login_password.value);
  Axios({
    method: 'post',
    url: `${window.location.origin}/api/login`,
    data: {
      email: login_email.value,
      password: login_password.value,
    }
  })
  .then(res => localStorage.setItem('token', res.data.token))
  .then(() => window.location.replace('/profile'))
  .catch((error) => {
    Swal.fire('', error.response.data.message, 'error')
  })
})

reg.addEventListener('submit', e => {
  let nameval, phonenum;
  e.preventDefault()
  
  if (name.value.length > 2) {
    nameval = name.value
  } else {
    Swal.fire('', 'Имя должно состоять минимум из 8 букв', 'error')
  }
  
  if (phone.value.length > 10) {
    phonenum = phone.value
  } else {
    Swal.fire('', 'Заполните поле телефон', 'error')
  }

  Axios({
    method: 'post',
    url: `${window.location.origin}/api/sign-up`,
    data: {
      username: nameval,
      phone: phonenum,
      email: email.value,
      password: password.value,
      password_repeat: repassword.value
    }
  }).then(res => {
      data = res.data
      localStorage.setItem('token', res.data.token)
    })
    .then(() => Swal.fire('', data.message, 'success').then(() => window.location.replace('/profile')))
    .catch((error) => {
      Swal.fire('', error.response.data.message, 'error')
    })
})

$(document).ready(function(){
  $('.phone').mask('+7-000-000-0000');
});

	$('.favorite-icon').click(function () {
        $('.fa-heart-o', this).toggleClass('fa-heart');  //Функция активной сердечки =)
		});

$('.banners').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  dots: false,
  arrows: false,
});

let profile = document.querySelectorAll('.profile')
let login_block = document.querySelector('.login_block')
let active = document.querySelector('.active')
let active_img = document.querySelector('.active_img')
let noactive = document.querySelector('.noactive')

login_block.style.display = 'none'

profile.forEach(item => {
  item.addEventListener('click', () => {
    if (localStorage.getItem('token') == null || localStorage.getItem('token' == 0)) {
      if (login_block.style.display === 'none') {
        login_block.style.display = 'block'
        active.style.display = 'block'
        active_img.style.display = 'block'
        noactive.style.display = 'none'
        console.log('ss');
      } else {
        login_block.style.display = 'none'
        active.style.display = 'none'
        active_img.style.display = 'none'
        noactive.style.display = 'block'
      }
    } else {
      window.location.replace('/profile')
    }
  })
})

let login = document.querySelector('.login')
let register = document.querySelector('.register')
let login_m = document.querySelector('.login_m')
let register_m = document.querySelector('.register_m')
let close = document.querySelector('.close')
// let favorite_page = document.querySelectorAll('.favorite_page')

// favorite_page.forEach(item => {
//   item.addEventListener('click', () => {
//     if (localStorage.getItem('token')) {
//       window.location.replace('/profile/favorite')
//     }
//   })
// })


login.addEventListener('click', () => {
  $('#login_modal').modal('show')
})

register.addEventListener('click', () => {
  $('#register_modal').modal('show')
})

login_m.addEventListener('click', () => {
  $('#register_modal').modal('hide')
  $('#login_modal').modal('show')
})

register_m.addEventListener('click', () => {
  $('#login_modal').modal('hide')
  $('#register_modal').modal('show')
})

$('.burger-menu-btn').click(function(e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
});

function init() {
  var myMap = new ymaps.Map("map", {
    center: [43.26141848, 76.94492184],
    zoom: 11
  }, {
    searchControlProvider: 'yandex#search'
  })
  var myPlacemark = new ymaps.Placemark(
    [43.26141848, 76.94492184]
  );
  myMap.geoObjects.add(myPlacemark);
}
ymaps.ready(init);

$('.slider-top').slick({
  infinite: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: false,
  autoplaySpeed: 5000,
  dots:true,
  arrows: false,
});

$('.discounts-slider').slick({
  infinite: true,
  slidesToShow: 3,
  slidesToScroll: 1,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-prev.png" alt="" class="slick-prev-btn">',
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
  ]
});

$('.images').slick({ // это изначально slick слайдер для основного блока изображений
  slidesToShow: 1,  // по одному слайдеру 
  slidesToScroll: 1, // по одному менять
  arrows:false, // включение стрелок (если не нужны false)
  asNavFor: '.imagesnew_dotted' // указываем что навигация для слайдера будет отдельно (указываем класс куда вешаем навигацию)
});

$('.imagesnew_dotted').slick({ // настройка навигации
  slidesToShow: 4, // указываем что нужно показывать 3 навигационных изображения
  asNavFor: '.images', // указываем что это навигация для блока выше
  focusOnSelect: true // указываем что бы слайделось по клику
});

let minus = document.querySelector('.minus')
let plus = document.querySelector('.plus')
let priceInfo = document.querySelector('.price-pay')
let priceCount = +priceInfo.textContent
let payment_count = document.querySelector('.payment_count')
if (plus) {
  plus.addEventListener('click', () => {
    payment_count.value = +payment_count.value + 1;
    let pricePlus = priceCount*payment_count.value;
    priceInfo.textContent = pricePlus
  })  
}

if (minus) {
  minus.addEventListener('click', () => {
    if(payment_count.value <= 1){
      console.log('error');
    }else{
      payment_count.value = +payment_count.value - 1;
      let pricePlus = priceCount*payment_count.value;
      priceInfo.textContent = pricePlus
    }
  })
}

var a = document.querySelector('.price-information').textContent;
var count = a.replace(/(\d)(?=(\d{3})+([^\d]|$))/g, "$1 ");

document.querySelector('.price-information').textContent = count