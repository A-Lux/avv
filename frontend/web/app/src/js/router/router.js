import Vue from 'vue'
import VueRouter from "vue-router";

import profile from './../components/profile/profile.vue';
import historyCart from './../components/profile/historyCart.vue';
import favorite from './../components/profile/favorite.vue';
import delivery from './../components/checkout/delivery.vue';
import pickup from './../components/checkout/pickup.vue';

Vue.use(VueRouter)

const routes = [
  {
    path: '/profile/history',
    name: 'history',
    component: historyCart
  },
  {
    path: '/profile',
    name: 'profile',
    component: profile
  },
  {
    path: '/profile/favorite',
    name: 'favorite',
    component: favorite
  },
  {
    path: '/basket/delivery',
    name: 'delivery',
    component: delivery
  },
  {
    path: '/basket/pickup',
    name: 'pickup',
    component: pickup
  },
]

const router = new VueRouter({
  routes,
  mode: "history"
})

export default router
