<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / О компании</span>
          <h2>О компании</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php include('sidebar.php') ?>
      </div>
      <div class="col-md-9">
        <div class="about_descr">
          <h2>AVV Industrial</h2>
          <div class="about_img">
            <img src="./images/about_img.png" alt="">
            <span class="about_img_descr_1">Lorem ipsum</span>
            <span class="about_img_descr_2">Lorem ipsum</span>
          </div>
          <p>Это комплексные поставки первоклассного высокотехнологического оборудования от лидирующих мировых производителей 
          для химической, перерабатывающей, биохимической, горнодобывающей, металлургической, текстильной, нефтегазодобывающей 
          и пищевой промышленности <strong>по всей территории Республики Казахстан.</strong></p>
          <h3>AVV Industrial – это:</h3>
          <div class="about_true">
            <div class="row">
              <div class="col-md-4 about_true_items">
                <img src="./images/done.png" alt="">
                <p>Профессиональное участие в шефмонтаже и пуско-наладке;</p>
              </div>
              <div class="col-md-4 about_true_items">
                <img src="./images/done.png" alt="">
                <p>Гарантийное и после гарантийное обслуживание;</p>
              </div>
              <div class="col-md-4 about_true_items">
                <img src="./images/done.png" alt="">
                <p>Разработка программ сервиса оборудования</p>
              </div>
            </div>
          </div>
          <div class="about_true_2">
            <div class="row">
              <div class="col-md-4 about_true_items">
                <img src="./images/done.png" alt="">
                <p>Инжиниринговый консалтинг, проектирование оптимальных к внедрению производственных решений</p>
              </div>
              <div class="col-md-4 about_true_items about_true_items_line">
                <img src="./images/done.png" alt="">
                <p>Все на условиях DDP (до дверей)</p>
              </div>
              <div class="col-md-4 about_true_items">
                <img src="./images/done.png" alt="">
                <p>На складе и под заказ.</p>
              </div>
            </div>
          </div>
          <p>В части реализации комплексных проектов мы предлагаем оборудование следующих производителей: <strong>SEKISUI, Zenit, Rowatti Pump,  Ari Armaturen и т.д.</strong></p>
          <p>Высокотехнологичные решения перечисленных производителей самодостаточны и позволяют реализовывать проекты практически любой объемности и сложности.</p>
          <p>Обработка проектных спецификаций и размещение заказа на начальном этапе не только экономит материальные средства и время, но и позволяет вовремя обнаружить возможные неточности, а порой и грубые ошибки, возникшие еще на этапе составления технического задания и проектирования.</p>
          <p>Своих Клиентов мы ориентируем на долгосрочное сотрудничество. Гибкая ценовая политика и профессионализм позволяют нам поддерживать долгосрочные партнерские отношения с нашими Клиентами.</p>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php include('footer.php') ?>