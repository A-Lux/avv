<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / </span><a href="/catalog">Каталог товаров</a><span> / </span><a href="#">Гидроаккумуляторы и мембранные баки</a><span> / </span><a href="#">Бойлеры косвенного нагрева</a> <span> / О компании</span>
          <h2>Бойлер STOUT напольный 200л 32 кВт косвенный нагрев</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php include('sidebar.php') ?>
      </div>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-4">
            <div class="images">
                <div><img src="./images/boiler.png" alt=""></div>
                <div><img src="./images/boiler.png" alt=""></div>
                <div><img src="./images/boiler.png" alt=""></div>
            </div>
            <div class="imagesnew_dotted" style="transform: none !important;"> // обязательно в css запретите трансформацию, а то будут иконки ездить в зависимости от номера слайда
                <img class="active" src="./images/boiler.png">
                <img src="./images/boiler.png">
                <img src="./images/boiler.png">
                <img src="./images/boiler.png">
            </div>
          </div>
          <div class="col-md-4 card_title_wrap">
            <div class="card_title">
              <div class="card_title_text">
                <h2>Артикул: SWH-1110-000200</h2>
              </div>
              <div class="star">
                <img src="./images/gold_star.png" alt="gold">
                <img src="./images/gold_star.png" alt="gold">
                <img src="./images/gold_star.png" alt="gold">
                <img src="./images/normal_star.png" alt="normal">
                <img src="./images/normal_star.png" alt="normal">
                <span>(1)</span>
              </div>
            </div>
            <div class="card_title_descr">
              <p><strong>Водонагреватель STOUT</strong> косвенного нагрева 200 литров 32 кВт напольный с муфтой 
              под ТЭН</p>
              <p>По своей сути рыбатекст является альтернативой традиционному lorem ipsum, который вызывает у некторых людей ...</p>
            </div>
            <div class="card_title_icons">
              <div class="card_title_icon_descr">
                <img src="./images/icon.png" alt="">
                <p>Бесплатная доставка по Москве <strong> от 15 000 руб.</strong></p>
              </div>
              <div class="card_title_icon_descr">
                <img src="./images/2.png" alt="">
                <p>Онлайн-консультации по любым вопросам</p>
              </div>
            </div>
            <div class="card_title_footer">
              <h3>Основные характеристики</h3>
              <div class="car_title_footer_descr">
                <div class="car_title_footer_row">
                  <div class="footer_h">Производитель</div>
                  <div class="footer_p">STOUT</div>
                </div>
                <div class="car_title_footer_row">
                  <div class="footer_h">Производство</div>
                  <div class="footer_p">Венгрия</div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card_payment">
              <div class="card_head">
                <h2>39 869,00 <span>руб. за 1 шт</span></h2>
              </div>
              <div class="card_payment_descr">
                <div class="card_payment_1">
                  <img src="./images/payment_check.png" alt="">
                  <span>В наличии</span>
                </div>
                <div class="card_payment_2">
                  <button class="minus">-</button>
                  <input class="payment_count" type="text" value="0" readonly="readonly">
                  <button class="plus">+</button>
                </div>
              </div>
              <div class="brands-favorite">
                <a href="">Добавить в корзину</a>
                <a href="" class="favorite-icon"><i class="fa fa-heart-o" aria-hidden="true"></i></a>
              </div>
              <div class="payment_variant">
                <h2>Способы оплаты</h2>
                <div class="payment_variant_img">
                  <img src="./images/visa.png" alt="">
                  <img src="./images/mir.png" alt="">
                  <img src="./images/mastercard.png" alt="">
                </div>
              </div>
              <div class="card_footer">
                <a href="#"><img src="./images/pay_icon.png" alt=""><span>Все способы оплаты</span></a>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="card_tabs">
              <nav>
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">
                    Описание
                  </a>
                  <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">
                    Характеристики
                  </a>
                  <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">
                    Вопросы и ответы (0)
                  </a>
                </div>
              </nav>
              <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                  <div class="card_tabs_descr">
                    <p><strong>Напольный бойлер STOUT SWH</strong> - надежный и удобный источник горячей воды в доме. Мощный теплообменник быстро нагревает воду и позволяет использовать бойлер в проточном режиме, без падения в нем температуры - до 690 литров в час! Внутренняя емкость и теплообменник защищены 
                      от коррозии высокопрочной стеклоэмалью и дополнительно - магниевым анодом.</p>
                    <h3>Особенности модели:</h3>
                    <ul>
                      <li>Встроенный термометр - удобно контролировать температуру.</li>
                      <li>Встроенный термостат - можно подключать <strong>ТЭН до 2,4 кВт 220В</strong> и регулировать в диапазоне <strong>5-65 С</strong>. Отличный вариант для "летнего режима" при отключенном котле.</li>
                      <li>Толстый слой теплоизоляции - меньше расходы на подогрев</li>
                      <li>Патрубок для подключения линии рециркуляции</li>
                    </ul>
                    <h3>Гарантия и обслуживание</h3>
                    <p>Гарантийный срок на основную емкость с теплообменником - <strong>60 месяцев</strong>, на электрические части - <strong>24 месяца</strong>. Для сохранения гарантии и продления срока службы нагревателя, рекомендуется менять магниевый анод не реже <strong>1 раза в год</strong>.</p>
                  </div>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                  <div class="card_tabs_descr">
                    <p><strong>Напольный бойлер STOUT SWH</strong> - надежный и удобный источник горячей воды в доме. Мощный теплообменник быстро нагревает воду и позволяет использовать бойлер в проточном режиме, без падения в нем температуры - до 690 литров в час! Внутренняя емкость и теплообменник защищены 
                      от коррозии высокопрочной стеклоэмалью и дополнительно - магниевым анодом.</p>
                    <h3>Особенности модели:</h3>
                    <ul>
                      <li>Встроенный термометр - удобно контролировать температуру.</li>
                      <li>Встроенный термостат - можно подключать <strong>ТЭН до 2,4 кВт 220В</strong> и регулировать в диапазоне <strong>5-65 С</strong>. Отличный вариант для "летнего режима" при отключенном котле.</li>
                      <li>Толстый слой теплоизоляции - меньше расходы на подогрев</li>
                      <li>Патрубок для подключения линии рециркуляции</li>
                    </ul>
                    <h3>Гарантия и обслуживание</h3>
                    <p>Гарантийный срок на основную емкость с теплообменником - <strong>60 месяцев</strong>, на электрические части - <strong>24 месяца</strong>. Для сохранения гарантии и продления срока службы нагревателя, рекомендуется менять магниевый анод не реже <strong>1 раза в год</strong>.</p>
                  </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                  <div class="card_tabs_descr">
                    <p><strong>Напольный бойлер STOUT SWH</strong> - надежный и удобный источник горячей воды в доме. Мощный теплообменник быстро нагревает воду и позволяет использовать бойлер в проточном режиме, без падения в нем температуры - до 690 литров в час! Внутренняя емкость и теплообменник защищены 
                      от коррозии высокопрочной стеклоэмалью и дополнительно - магниевым анодом.</p>
                    <h3>Особенности модели:</h3>
                    <ul>
                      <li>Встроенный термометр - удобно контролировать температуру.</li>
                      <li>Встроенный термостат - можно подключать <strong>ТЭН до 2,4 кВт 220В</strong> и регулировать в диапазоне <strong>5-65 С</strong>. Отличный вариант для "летнего режима" при отключенном котле.</li>
                      <li>Толстый слой теплоизоляции - меньше расходы на подогрев</li>
                      <li>Патрубок для подключения линии рециркуляции</li>
                    </ul>
                    <h3>Гарантия и обслуживание</h3>
                    <p>Гарантийный срок на основную емкость с теплообменником - <strong>60 месяцев</strong>, на электрические части - <strong>24 месяца</strong>. Для сохранения гарантии и продления срока службы нагревателя, рекомендуется менять магниевый анод не реже <strong>1 раза в год</strong>.</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="about_products">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="title_card">Похожие товары</div>
      </div>
      <div class="col-xl-3 col-md-6 card_d">
        <div class="brands-inner">
            <div class="brands-hashtags">
                <div class="new-brands">
                    <p>New</p>
                </div>
                <div class="hit-brands">
                    <p>Хит</p>
                </div>
                <div class="stock-brands">
                    <p>В наличии</p>
                </div>
            </div>
            <div class="brands-image">
                <img src="images/brands1.png" alt="">
            </div>
            <div class="brands-info">
                <h1>Циркуляционный насос Wilo Star-e</h1>
                <p>Артикуль: 12345678</p>
                <div class="brands-stars">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                <h1 class="brands-price">43 323 руб./шт</h1>
            </div>
            <div class="brands-favorite">
                <a href="">Добавить в корзину</a>
                <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                        aria-hidden="true"></i></a>
            </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-6 card_d">
        <div class="brands-inner">
          <div class="brands-hashtags">
              <div class="new-brands">
                  <p>New</p>
              </div>
              <div class="hit-brands">
                  <p>Хит</p>
              </div>
              <div class="stock-brands">
                  <p>В наличии</p>
              </div>
          </div>
          <div class="brands-image">
              <img src="images/brands1.png" alt="">
          </div>
          <div class="brands-info">
              <h1>Циркуляционный насос Wilo Star-e</h1>
              <p>Артикуль: 12345678</p>
              <div class="brands-stars">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
              <h1 class="brands-price">43 323 руб./шт</h1>
          </div>
          <div class="brands-favorite">
              <a href="">Добавить в корзину</a>
              <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                      aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-6 card_d">
        <div class="brands-inner">
          <div class="brands-hashtags">
              <div class="new-brands">
                  <p>New</p>
              </div>
              <div class="hit-brands">
                  <p>Хит</p>
              </div>
              <div class="stock-brands">
                  <p>В наличии</p>
              </div>
          </div>
          <div class="brands-image">
              <img src="images/brands1.png" alt="">
          </div>
          <div class="brands-info">
              <h1>Циркуляционный насос Wilo Star-e</h1>
              <p>Артикуль: 12345678</p>
              <div class="brands-stars">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
                  <img src="images/star.png" alt="">
              </div>
              <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
              <h1 class="brands-price">43 323 руб./шт</h1>
          </div>
          <div class="brands-favorite">
              <a href="">Добавить в корзину</a>
              <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                      aria-hidden="true"></i></a>
          </div>
        </div>
      </div>
      <div class="col-xl-3 col-md-6 card_d">
          <div class="brands-inner">
            <div class="brands-hashtags">
                <div class="new-brands">
                    <p>New</p>
                </div>
                <div class="hit-brands">
                    <p>Хит</p>
                </div>
                <div class="stock-brands">
                    <p>В наличии</p>
                </div>
            </div>
            <div class="brands-image">
                <img src="images/brands1.png" alt="">
            </div>
            <div class="brands-info">
                <h1>Циркуляционный насос Wilo Star-e</h1>
                <p>Артикуль: 12345678</p>
                <div class="brands-stars">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                    <img src="images/star.png" alt="">
                </div>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                <h1 class="brands-price">43 323 руб./шт</h1>
            </div>
            <div class="brands-favorite">
                <a href="">Добавить в корзину</a>
                <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                        aria-hidden="true"></i></a>
            </div>
        </div>
      </div>
    </div>
  </div>
</div>
<?php include('footer.php') ?>