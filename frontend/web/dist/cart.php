<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / Корзина</span>
          <h2>Корзина</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php include('sidebar.php') ?>
      </div>
      <div class="col-md-9">
        <div id="app">
          <Cart></Cart>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php include('footer.php') ?>