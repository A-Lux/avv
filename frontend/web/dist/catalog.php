<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span>/ <a href="/catalog">Каталог товаров</a> / Гидроаккумуляторы и мембранные баки</span>
          <h2>Гидроаккумуляторы и мембранные баки</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php include('sidebar.php') ?>
      </div>
      <div class="col-md-9">
        <div id="app">
          <Catalog></Catalog>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php include('footer.php') ?>