<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / Оформление заказа</span>
          <h2>Оформление заказа</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div id="app">
          <Checkout></Checkout>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php include('footer.php') ?>