<?php include('header.php') ?>
<div class="about">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="navigation_links">
          <a href="/">Главная</a><span> / Контакты</span>
          <h2>Контакты</h2>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-3">
        <?php include('sidebar.php') ?>
      </div>
      <div class="col-md-9">
        <div class="row">
          <div class="col-md-3">
            <div class="contact">
              <h2>Алматы</h2>
              <h3>ТОО "AVV Industrial"</h3>
              <div class="contact_descr">
                <div class="contact_icon">
                  <img src="./images/phone_icon.png" alt="">
                  <span>Телефоны</span>
                </div>
                <div class="contact_text">
                  <p>+7 (727) 390 23 30</p>
                  <p>+7 (727) 390 23 30</p>
                </div>
              </div>
              <div class="contact_descr">
                <div class="contact_icon">
                  <img src="./images/phone_icon.png" alt="">
                  <span>Факс</span>
                </div>
                <div class="contact_text">
                  <p>+7 (727) 390 23 30</p>
                </div>
              </div>
              <div class="contact_descr">
                <div class="contact_icon">
                  <img src="./images/geo.png" alt="">
                  <span>Адрес</span>
                </div>
                <div class="contact_text">
                  <p>050061, Республика Казахстан, г. Алматы, мкр. Курылысши, ул. Молодёжная, дом 2А, офис 203 (БЦ Массагет)</p>
                </div>
              </div>
              <div class="contact_descr">
                <div class="contact_icon">
                  <img src="./images/message.png" alt="">
                  <span>Телефоны</span>
                </div>
                <div class="contact_text">
                  <a href="#">info@avv.kz</a>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-9 contact_wrap">
            <div class="contact_map">
              <div style="position:relative;overflow:hidden;">
                <a href="https://yandex.kz/maps/162/almaty/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Алматы</a>
                <a href="https://yandex.kz/maps/162/almaty/?ll=76.945465%2C43.238293&utm_medium=mapframe&utm_source=maps&z=12" style="color:#eee;font-size:12px;position:absolute;top:14px;">Алматы — Яндекс.Карты</a>
                <iframe src="https://yandex.kz/map-widget/v1/-/CBR1Y-Un1A" width="100%" height="373px" frameborder="1" allowfullscreen="true" style="position:relative; border: none;"></iframe>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-12">
            <div class="contact_form">
              <h2>Давайте вместе решим вашу проблему! Заполните форму обратной связи</h2>
              <img src="./images/contact_machine.png" alt="">
              <div class="row">
                <div class="col-md-8">
                  <form>
                    <div class="contact_form_inputs">
                      <input type="text" placeholder="Имя">
                      <input type="text" placeholder="E-mail">
                    </div>
                    <div class="contact_form_textarea">
                      <textarea name="" id="" cols="55" rows="10" placeholder="Сообщение"></textarea>
                    </div>
                    <label class="form_checkbox">
                      <input type="checkbox">
                      <span class="check"><img src="./images/check.png" alt=""></span>
                      <span class="checkmark">При отправке данной формы Вы подтверждаете свою дееспособность и согласие на <a href="#">обработку персональных данных.</a></span>
                    </label>
                    <button class="contact_btn" type="submit">Отправить</button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>  
</div>
<?php include('footer.php') ?>