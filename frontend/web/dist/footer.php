<div class="footer">
        <div class="footer-top">
            <div class="container">
            <div class="footer-links">
                <nav>
                    <li>
                        <a href="">О компании</a>
                    </li>
                    <li>
                        <a href="">Бренды</a>
                    </li>
                    <li>
                        <a href="">Контакты</a>
                    </li>
                </nav>
            </div>
            </div>
        </div>
        <div class="footer-medium">
            <div class="container">
            <div class="row">
                <div class="col-xl-4 col-md-4">
                    <div class="footer-info">
                        <h1>Подписаться на рассылку</h1>
                        <p>Вы всегда будете в крусе лучших наших пердложений</p>
                        <input type="text" placeholder='Введите e-mail'>
                        <a href="">Подписаться</a>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="footer-info-contact">
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/mail-footer.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Напишите нам </p>
                                <a href="">info@avv.kz</a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/phone.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Звоните нам </p>
                                <a href="">+7 (727) 390 20 90</a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/fax.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Факс </p>
                                <a href="">+7 (727) 243 53 38</a>
                            </div>
                        </div>
                        <div class="footer-links">
                            <div class="footer-image">
                                <img src="images/map.png" alt="">
                            </div>
                            <div class="footer-text">
                                <p>Адрес </p>
                                <span>г. Алматы, мкр.Курылысши,ул.  Молодёжная, дом 2А, офис 203</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4 col-md-4">
                    <div class="footer-info">
                        <div id="map">
                            
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-md-4 col-2">
                        <div class="footer-bottom-info">
                            <img src="images/footer-logo.png" alt="">
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 col-10">
                        <div class="footer-bottom-info">
                            <p>© 2017 - 2020  AVV industrial. Все праа защищенты</p>
                        </div>
                    </div>
                    <div class="col-xl-4 col-md-4 col-12">
                        <div class="footer-bottom-info social-icons">
                            <a href=""><i class="fa fa-instagram" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-facebook" aria-hidden="true"></i></a>
                            <a href=""><i class="fa fa-vk" aria-hidden="true"></i></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
<script src="js/main.js"></script>
</body>
</html>