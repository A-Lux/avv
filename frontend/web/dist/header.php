<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.1/css/fontawesome.min.css"> -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/main.css">
</head>

<body>
    <div class="header">
        <div class="container-fluid">
            <div class="header-inner">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="header-links">
                            <nav>
                                <li class="logo">
                                    <a href="">
                                        <img src="images/logo.png" alt="">
                                    </a>
                                </li>
                                <li>
                                    <a href="">О компании</a>
                                </li>
                                <li>
                                    <a href="">Бренды</a>
                                </li>
                                <li>
                                    <a href="">Контакты</a>
                                </li>
                            </nav>
                        </div>
                    </div>
                    <div class="col-xl-3 pr-0">
                        <div class="header-info-inner">
                            <div class="header-info-box">
                                <div class="header-icon">
                                    <img src="images/mail.svg" alt="">
                                </div>
                                <div class="header-text">
                                    <p>Напишите нам</p>
                                    <a> info@avv.kz</a>
                                </div>
                            </div>
                            <div class="header-info-box">
                                <div class="header-icon">
                                    <img src="images/phone.svg" alt="">
                                </div>
                                <div class="header-text">
                                    <p>Звоните нам </p>
                                    <a> +7 (727) 390 20 90</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-2 pr-0 col-12">
                        <div class="header-icons-inner">
                            <a href="#" class="header-icons profile">
                                <img class="noactive" src="images/Profile.svg" alt="">
                                <img class="active" src="images/profile_red.svg" alt="">
                            </a>
                            <a href="#" class="header-icons search">
                                <img src="images/search.svg" alt="">
                            </a>
                            <a href="#" class="header-icons">
                                <img src="images/favorite.svg" alt="">
                            </a>
                            <a href="#" class="header-icons">
                                <img src="images/basket shop.svg" alt="">
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="burger-menu-content">
            <a href="index.php">
                <img src="images/logo.png" alt="">
            </a>
            <a href="#" class="header-icons header-icons-mob profile">
                <img class="noactive" src="images/Profile.svg" alt="">
                <img class="active" src="images/profile_red.svg" alt="">
            </a>
            <a href="#" class="header-icons header-icons-mob search">
                <img src="images/search.svg" alt="">
            </a>
            <a href="#" class="header-icons header-icons-mob">
                <img src="images/basket shop.svg" alt="">
            </a>
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>
        <div class="nav-panel-mobil">
            <div class="container">
                <nav class="navbar-expand-lg navbar-light">
                    <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                        <li class="nav-item">
                            <a href=""><img src="images/home-image.png" class="home-image" alt="">Магазин</a>
                        </li>
                        <li class="nav-item">
                            <a href="">О компании</a>
                        </li>
                        <li class="nav-item">
                            <a href="">Контакты</a>
                        </li>
                        <li class="nav-item">
                            <a href="">Бренды</a>
                        </li>
                        <div class="header-info-box">
                            <div class="header-icon">
                                <img src="images/mail.svg" alt="">
                            </div>
                            <div class="header-text">
                                <p>Напишите нам</p>
                                <a> info@avv.kz</a>
                            </div>
                        </div>
                        <div class="header-info-box">
                            <div class="header-icon">
                                <img src="images/phone.svg" alt="">
                            </div>
                            <div class="header-text">
                                <p>Звоните нам </p>
                                <a> +7 (727) 390 20 90</a>
                            </div>
                        </div>
                    </ul>
                </nav>
            </div>
        </div>
        <div class="login_block">
            <h3>Личный кабинет</h3>
            <div class="login_btns">
                <a class="login" href="#">Войти</a>
                <a class="register" href="#">Зарегистрироваться</a>
            </div>
        </div>

        <!-- Login modal -->
        <div class="modal" id="login_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width: 358px; margin: 0 auto; margin-top: 75px; margin-bottom: 40px;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Войти</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3>E-mail</h3>
                        <input type="text">
                        <h3>Пароль</h3>
                        <input type="text">
                    </div>
                    <div class="modal-footer">
                        <a class="modal_btn" href="#">Войти</a>
                        <a class="modal_a register_m" href="#">Регистрация</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Register modal -->
        <div class="modal" id="register_modal" tabindex="-1" role="dialog">
            <div class="modal-dialog" style="width: 358px; margin: 0 auto; margin-top: 25px;" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">регистрация</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3>Имя</h3>
                        <input type="text">
                        <h3>Фамилия</h3>
                        <input type="text">
                        <h3>Телефон</h3>
                        <input type="text">
                        <h3>E-mail</h3>
                        <input type="text">
                        <h3>Пароль</h3>
                        <input type="text">
                        <h3>Повторите пароль</h3>
                        <input type="text">
                    </div>
                    <div class="modal-footer">
                        <a class="modal_btn" href="#">Зарегистрироваться</a>
                        <a class="modal_a login_m" href="#">Войти в аккаунт</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
