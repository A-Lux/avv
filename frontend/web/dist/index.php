<?php include('header.php') ?>
<div class="home">
    <div class="container">
        <div class="slider-top">
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
            <div class="home-slide">
                <div class="home-slide-text">
                    <h1>Цыркуляционные насосы <br> Dab - выбор професионалов! </h1>
                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Sunt libero eum iusto inventore
                        exercitationem asperiores excepturi repudiandae suscipit accusamus pariatur!</p>
                    <a href="">Подробнее</a>
                </div>
            </div>
        </div>
        <div class="title">
            <h1>Каталог товаров</h1>
            <a href="#">смотреть все</a>
        </div>
        <div class="catalog-inner">
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Котельное оборудование</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Фитинги для труб</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Запорная труба и регулирующая</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Радиаторы отопления</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link catalog-right">
                    <a href="" class="catalog-title">Насосное оборудование</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Бойлеры и водонагреватели </a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Трубы и шланги</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Предохранительная арматура</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Канализацонные трубы</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link catalog-right">
                    <a href="" class="catalog-title">Крепож и герметики</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Баки, гиадраккамуляторы</a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Фильтрация и водоподготовка </a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">КИП и автоматика </a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link">
                    <a href="" class="catalog-title">Инструменты и акссесуары </a>
                </div>
            </a>
            <a href="">
                <div class="catalog-link catalog-right">
                    <a href="" class="catalog-title">Предохранительная арматура</a>
                </div>
            </a>
        </div>
        <div class="title">
            <h1>Акции и скидки</h1>
        </div>
        <div class="discounts-slider">
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis1.png" alt="">
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>30% скидка на доставку СДЭК</p>
                    </div>
                </div>
            </a>
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis2.png" alt="">
                        <div class="discount-info">
                            <h1>СКИДКА от 20% до 50%</h1>
                            <p>Распродажа складских остатоков</p>
                        </div>
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>30% скидка на доставку СДЭК</p>
                    </div>
                </div>
            </a>
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis3.png" alt="">
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>Бесплатная доставка в Регионы</p>
                    </div>
                </div>
            </a>
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis1.png" alt="">
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>30% скидка на доставку СДЭК</p>
                    </div>
                </div>
            </a>
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis2.png" alt="">
                        <div class="discount-info">
                            <h1>СКИДКА от 20% до 50%</h1>
                            <p>Распродажа складских остатоков</p>
                        </div>
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>30% скидка на доставку СДЭК</p>
                    </div>
                </div>
            </a>
            <a href="">
                <div class="discount-slide">
                    <div class="discount-image">
                        <img src="images/dis3.png" alt="">
                    </div>
                    <div class="discount-content">
                        <h1>Действует до 30 апреля 2020 </h1>
                        <p>Бесплатная доставка в Регионы</p>
                    </div>
                </div>
            </a>
        </div>
    </div>
    <div class="products">
        <div class="container">
            <div class="row">
                <div class="col-xl-6">
                    <div class="title">
                        <h1>Товары от бренда</h1>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="list-group" id="list-tab" role="tablist">
                        <a class="list-group-item list-group-item-action active" id="list-brands-one-list"
                            data-toggle="list" href="#list-brands-one" role="tab" aria-controls="brands-one">Бренд 1</a>
                        <a class="list-group-item list-group-item-action" id="list-brands-two-list" data-toggle="list"
                            href="#list-brands-two" role="tab" aria-controls="brands-two">Бренд 2</a>
                        <a class="list-group-item list-group-item-action" id="list-brands-three-list" data-toggle="list"
                            href="#list-brands-three" role="tab" aria-controls="brands-three">Бренд 3</a>
                    </div>
                </div>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-brands-one" role="tabpanel"
                    aria-labelledby="list-brands-one-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-brands-two" role="tabpanel" aria-labelledby="list-brands-two-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-brands-three" role="tabpanel"
                    aria-labelledby="list-brands-three-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="popular-products">
        <div class="container">
            <div class="list-group" id="list-tab" role="tablist">
                <a class="list-group-item list-group-item-action " id="list-products-one-list" data-toggle="list"
                    href="#list-products-one" role="tab" aria-controls="products-one">Специальное предложения</a>
                <a class="list-group-item list-group-item-action active" id="list-products-two-list" data-toggle="list"
                    href="#list-products-two" role="tab" aria-controls="products-two">Популярные товары</a>
                <a class="list-group-item list-group-item-action" id="list-products-three-list" data-toggle="list"
                    href="#list-products-three" role="tab" aria-controls="products-three">Новинки</a>
                <a class="list-group-item list-group-item-action" id="list-products-three-list" data-toggle="list"
                    href="#list-products-four" role="tab" aria-controls="products-four">Хит продаж</a>
            </div>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="list-products-one" role="tabpanel"
                    aria-labelledby="list-products-one-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-products-two" role="tabpanel"
                    aria-labelledby="list-products-two-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="list-products-three" role="tabpanel"
                    aria-labelledby="list-products-three-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
                <div class="tab-pane fade" id="list-products-three" role="tabpanel"
                    aria-labelledby="list-products-four-list">
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="brands-inner">
                                <div class="brands-hashtags">
                                    <div class="new-brands">
                                        <p>New</p>
                                    </div>
                                    <div class="hit-brands">
                                        <p>Хит</p>
                                    </div>
                                    <div class="stock-brands">
                                        <p>В наличии</p>
                                    </div>
                                </div>
                                <div class="brands-image">
                                    <img src="images/brands1.png" alt="">
                                </div>
                                <div class="brands-info">
                                    <h1>Циркуляционный насос Wilo Star-e</h1>
                                    <p>Артикуль: 12345678</p>
                                    <div class="brands-stars">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                        <img src="images/star.png" alt="">
                                    </div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed </p>
                                    <h1 class="brands-price">43 323 руб./шт</h1>
                                </div>
                                <div class="brands-favorite">
                                    <a href="">Добавить в корзину</a>
                                    <a href="" class="favorite-icon"><i class="fa fa-heart-o"
                                            aria-hidden="true"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
    <div class="shop-avv">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12">
                    <div class="title">
                        <h1>Магазин Avv industrial</h1>
                    </div>
                    <div class="shop-text">
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Eaque expedita atque aspernatur
                            vero, mollitia ea! Quas saepe a reprehenderit magni voluptate corporis amet, commodi fugit
                            asperiores doloribus at consequuntur animi.</p>
                        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Fuga, modi perferendis facere
                            mollitia consequuntur minus dolorum commodi rem nisi accusamus porro quia reprehenderit a
                            quo velit totam hic saepe alias facilis enim at voluptate voluptatibus. Aut assumenda est
                            rerum exercitationem.</p>
                        <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Recusandae illo tempore dolorem
                            totam tempora maxime sed aliquam corporis. Eligendi incidunt ipsa accusantium. Asperiores
                            impedit eligendi, mollitia nostrum aut consequatur fugiat!</p>
                    </div>
                </div>
                <div class="col-xl-6 col-12">
                    <div class="shop-image">
                        <img src="images/shop.png" alt="">
                        <span class="shop-text-1">Lorem ipsum</span>
                        <span class="shop-text-2">Lorem ipsum</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php') ?>