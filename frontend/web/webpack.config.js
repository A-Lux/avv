const path = require('path');
const VueLoaderPlugin = require("vue-loader/lib/plugin");

module.exports = {
    mode: "development",
    entry: ['./app/src/js/app.js', './app/src/js/main.js'],
    output: {
        filename: 'main.js',
        path: path.join(__dirname, './dist/js')
    },
    module: {
        rules: [
            {
                test: /\.js$/,
                loader: "babel-loader",
            },
            {
                test: /\.vue$/,
                loader: "vue-loader",
            },
            {
                test: /\.css$/i,
                use: ['style-loader', 'css-loader'],
            },
            {
                test: /\.scss$/,
                use: ["vue-style-loader", "css-loader", "sass-loader"],
            },
            // {
            //     test: /\.(scss)$/,
            //     use: [
            //     {
            //         loader: 'style-loader'
            //     },
            //     {
            //         loader: 'css-loader'
            //     },
            //     {
            //         loader: 'vue-style-loader'
            //     },
            //     {
            //         loader: 'postcss-loader',
            //         options: {
            //         plugins: function () {
            //             return [
            //             require('autoprefixer')
            //             ];
            //         }
            //         }
            //     },
            //     {
            //         loader: 'sass-loader'
            //     }
            //     ]
            // }
        ]
    },
    plugins: [new VueLoaderPlugin()],
    resolve: {
        alias: {
        vue: "vue/dist/vue.js",
        },
    },
};
